﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("EVolution Apps")> 
<Assembly: AssemblyDescription("EVolution Apps")> 
<Assembly: AssemblyCompany("rafatux.com")> 
<Assembly: AssemblyProduct("EVA")> 
<Assembly: AssemblyCopyright("RafaTux")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c3f69772-6778-4783-9123-c149d6031729")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("19.03.11.01")>
<Assembly: AssemblyFileVersion("19.03.11.01")>

<Assembly: NeutralResourcesLanguageAttribute("es-ES")> 