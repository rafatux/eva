﻿Option Explicit On
Option Strict On
Imports System.Management
Imports System.Reflection.Assembly
Imports System.IO

Namespace FrameWork
    Public Class cls_fotografix
        Dim _oSistema As New eva.FrameWork.cls_USF
        Dim _oArchivo As New eva.FrameWork.cls_UAF
        Private _oLog As New eva.FrameWork.cls_Log

        Public Sub New()
            Try
                Dim assembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
                Dim root As String = assembly.GetName().Name
                Dim stream As System.IO.Stream = assembly.GetManifestResourceStream(root + "." + "fotografix15.zip")
                Dim buffer(Convert.ToInt32(stream.Length) - 1) As Byte
                stream.Read(buffer, 0, buffer.Length)
                stream.Close()
                Dim f As New IO.FileStream(_oSistema.ruta_carpeta_temporal & "\Fotografix.zip", IO.FileMode.Create, IO.FileAccess.Write)
                f.Write(buffer, 0, buffer.Length)
                f.Close()
                _oArchivo.Ruta = _oSistema.ruta_carpeta_temporal & "\Fotografix.zip"
                _oArchivo.UnZIP(_oSistema.ruta_carpeta_temporal)
            Catch ex As Exception
                _oLog.Log("Error: Extrayendo paquete fotografix")
            End Try
            
        End Sub

        Public Sub editar(RutaImagen As String)
            Try
                Process.Start(_oSistema.ruta_carpeta_temporal & "\fotografix15\Fotografix.exe", RutaImagen)
            Catch ex As Exception
                _oLog.Log("Error: Ejecutando el paquete fotografix")
            End Try

        End Sub


    End Class
End Namespace

