﻿Option Explicit On
Option Strict On
Imports System.Net
Imports System.Net.Mail

Namespace FrameWork
    ''' <summary>
    ''' Gestión de envio de correos electrónicos.
    ''' </summary>
    ''' <remarks>
    ''' Actualmente premite el envio de correo electrónicos y está preparado para servidores google standard.
    ''' 
    ''' Los emails son codificado automáticamente en UTF-8 en el proceso del envio.
    ''' </remarks>
    Public Class cls_EF
        ''' <summary>
        ''' Campo interno de email del emisor
        ''' </summary>
        Private _sEmailDe As String = ""
        ''' <summary>
        ''' Campo interno de email del destinatario
        ''' </summary>
        Private _sEmailPara As String = ""
        ''' <summary>
        ''' Campo interno de url del servidor de envio
        ''' </summary>
        Private _sSmtp As String = ""

        Private _nPuerto As Integer = 0
        ''' <summary>
        ''' Campo interno de usuario del emisor
        ''' </summary>
        Private _sUsuario As String = ""
        ''' <summary>
        ''' Campo interno de password del emisor
        ''' </summary>
        Private _sPassword As String = ""
        ''' <summary>
        ''' Campo interno de asunto del email
        ''' </summary>
        Private _sAsunto As String = ""
        ''' <summary>
        ''' Campo interno de mensajes de correo
        ''' </summary>
        Private _sMensaje As String = ""
        ''' <summary>
        ''' Campo interno de estado del envio
        ''' </summary>
        ''' 
        Private _bSSL As Boolean = False
        Private _bEnviado As Boolean = False
        Private _sMensajeError As String = ""

        ''' <summary>
        ''' Indica el correo electrónico del origen.
        ''' </summary>
        ''' <value>Dirección completa del emisor.</value>
        ''' <returns>""</returns>
        ''' <remarks></remarks>
        Public Property De() As String
            Get
                Return _sEmailDe
            End Get
            Set(ByVal value As String)
                _sEmailDe = value
            End Set
        End Property

        ''' <summary>
        ''' Indica el correo electrónico del destinatario.
        ''' </summary>
        ''' <value>Dirección completa del destinatario.</value>
        ''' <returns>""</returns>
        ''' <remarks></remarks>
        Public Property Para() As String
            Get
                Return _sEmailPara
            End Get
            Set(ByVal value As String)
                _sEmailPara = value
            End Set
        End Property

        ''' <summary>
        ''' Url del servidor smtp.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Smtp() As String
            Get
                Return _sSmtp
            End Get
            Set(ByVal value As String)
                _sSmtp = value
            End Set
        End Property

        Public Property Puerto As Integer
            Get
                Return _nPuerto
            End Get
            Set(value As Integer)
                _nPuerto = value
            End Set
        End Property

        ''' <summary>
        ''' Usuario para el servidor smtp
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Usuario() As String
            Get
                Return _sUsuario
            End Get
            Set(ByVal value As String)
                _sUsuario = value
            End Set
        End Property

        ''' <summary>
        ''' Password del usuario para el servidor smtp.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Password() As String
            Get
                Return _sPassword
            End Get
            Set(ByVal value As String)
                _sPassword = value
            End Set
        End Property

        ''' <summary>
        ''' Asunto del correo electrónico.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Asunto() As String
            Get
                Return _sAsunto
            End Get
            Set(ByVal value As String)
                _sAsunto = value
            End Set
        End Property

        ''' <summary>
        ''' Mensaje enviado en el cuerpo electrónico en texto plano o html
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Mensaje() As String
            Get
                Return _sMensaje
            End Get
            Set(ByVal value As String)
                _sMensaje = value
            End Set
        End Property

        Public Property SSL As Boolean
            Get
                Return _bSSL
            End Get
            Set(value As Boolean)
                _bSSL = True
            End Set
        End Property

        ''' <summary>
        ''' Si ha posido ser enviado el corre electrónico.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Verdadero/Falso</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Enviado() As Boolean
            Get
                Return _bEnviado
            End Get
        End Property

        Public ReadOnly Property MensajeError As String
            Get
                Return _sMensajeError
            End Get
        End Property

        ''' <summary>
        ''' Procesa el correo electrónico siguiendo los valores de las propiedades.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function enviar() As Boolean
            Dim correo As MailMessage = New MailMessage(_sEmailDe, _sEmailPara)
            Dim Clientesmtp As SmtpClient

            correo.Subject = _sAsunto
            correo.SubjectEncoding = System.Text.Encoding.UTF8
            correo.Body = _sMensaje
            correo.BodyEncoding = System.Text.Encoding.UTF8

            If _nPuerto = 0 Then
                Clientesmtp = New SmtpClient(_sSmtp)
            Else
                Clientesmtp = New SmtpClient(_sSmtp, _nPuerto)
            End If

            Try
                Clientesmtp.UseDefaultCredentials = False
                Clientesmtp.Credentials = New System.Net.NetworkCredential(_sUsuario, _sPassword)

                Clientesmtp.Timeout = 20000
                Clientesmtp.EnableSsl = _bSSL

                Clientesmtp.Send(correo)
                _bEnviado = True
                _sMensajeError = ""
                Return _bEnviado

            Catch ex As Exception
                _sMensajeError = ex.Message
                _bEnviado = False
                Return _bEnviado
            End Try
        End Function

        ''' <summary>
        ''' Contructor de la clase.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()

        End Sub

    End Class
End Namespace