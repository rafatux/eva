﻿Imports eva
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System

Namespace FrameWork
    ''' <remarks>Tratamiento de archivos de imágenes</remarks>
    Public Class cls_UIF
        Private _oLog As New eva.FrameWork.cls_Log

        ''' <summary>
        ''' Convierte un archivo de imágen a formato TIFF
        ''' </summary>
        ''' <param name="ruta_archivo_origen">Ruta del archivo origen con su extensión.</param>
        ''' <param name="ruta_archivo_destino">Ruta del archivo destino con su extensión TIFF</param>
        ''' <returns>Verdadero si se ha convertido correctamente.</returns>
        ''' <remarks></remarks>
        Public Function Convertir_a_tif(ByVal ruta_archivo_origen As String, ByVal ruta_archivo_destino As String) As Boolean
            Dim _bResultado As Boolean = False
            Dim _oImagen As Bitmap
            Dim _oArchivo_origen As New eva.FrameWork.cls_UAF
            Dim _oarchivo_destino As New eva.FrameWork.cls_UAF

            _oArchivo_origen.Ruta = ruta_archivo_origen
            _oarchivo_destino.Ruta = ruta_archivo_destino

            Try
                If _oarchivo_destino.Existe Then
                    _oarchivo_destino.Borrar()
                End If

                If _oArchivo_origen.Existe Then
                    _oImagen = New Bitmap(ruta_archivo_origen)
                    _oImagen.Save(ruta_archivo_destino, ImageFormat.Tiff)
                End If
            Catch ex As Exception
                _oLog.Log("Error: Conversión a tif: " & ex.Message)
            End Try
            
            Return _bResultado
        End Function

        Public Function Convertir_a_jpg(ByVal ruta_archivo_origen As String, ByVal ruta_archivo_destino As String) As Boolean
            Dim _bResultado As Boolean = False
            Dim _oImagen As Bitmap
            Dim _oArchivo_origen As New eva.FrameWork.cls_UAF
            Dim _oarchivo_destino As New eva.FrameWork.cls_UAF

            _oArchivo_origen.Ruta = ruta_archivo_origen
            _oarchivo_destino.Ruta = ruta_archivo_destino

            Try
                If _oarchivo_destino.Existe Then
                    _oarchivo_destino.Borrar()
                End If

                If _oArchivo_origen.Existe Then
                    _oImagen = New Bitmap(ruta_archivo_origen)
                    _oImagen.Save(ruta_archivo_destino, ImageFormat.Jpeg)
                End If
            Catch ex As Exception
                _oLog.Log("Error: Conversión a jpg: " & ex.Message)
            End Try
            
            Return _bResultado
        End Function

        Public Function Convertir_a_png(ByVal ruta_archivo_origen As String, ByVal ruta_archivo_destino As String) As Boolean
            Dim _bResultado As Boolean = False
            Dim _oImagen As Bitmap
            Dim _oArchivo_origen As New eva.FrameWork.cls_UAF
            Dim _oarchivo_destino As New eva.FrameWork.cls_UAF

            _oArchivo_origen.Ruta = ruta_archivo_origen
            _oarchivo_destino.Ruta = ruta_archivo_destino

            Try
                If _oarchivo_destino.Existe Then
                    _oarchivo_destino.Borrar()
                End If

                If _oArchivo_origen.Existe Then
                    _oImagen = New Bitmap(ruta_archivo_origen)
                    _oImagen.Save(ruta_archivo_destino, ImageFormat.Png)
                End If
            Catch ex As Exception
                _oLog.Log("Error: Conversión a png: " & ex.Message)
            End Try
            
            Return _bResultado
        End Function

    End Class
End Namespace
