﻿
Option Explicit On
Option Strict On
Imports System.Data

Namespace FrameWork
    Public Class cls_ADNNF
        Dim _oBaseDatos As New DataSet
        Dim _nPuntero As Integer

        Public ReadOnly Property BaseDatos As DataSet
            Get
                Return _oBaseDatos
            End Get
        End Property

        Public Property Puntero As Integer
            Get
                Return _nPuntero
            End Get
            Set(value As Integer)
                _nPuntero = value
            End Set
        End Property

        Public Sub Reinicar_Base_de_datos()
            _oBaseDatos.Reset()
        End Sub

        Public ReadOnly Property Comparecientes As DataTable
            Get
                Return _oBaseDatos.Tables("Comparecientes")
            End Get
        End Property

        Public ReadOnly Property Conceptos As DataTable
            Get
                Return _oBaseDatos.Tables("Conceptos")
            End Get
        End Property

        Public ReadOnly Property Protocolo As DataTable
            Get
                Return _oBaseDatos.Tables("Protocolo")
            End Get
        End Property

        ''' <summary>
        ''' Crea la estructura de comparecientes.
        ''' </summary>
        Private Sub Crea_Tabla_Comparecientes()
            Dim _oColumna As DataColumn

            _oBaseDatos.Tables.Add("Comparecientes")

            _oColumna = New DataColumn
            _oColumna.ColumnName = "ID"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 15
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Tipo_Documento"
            _oColumna.DataType = System.Type.GetType("System.Byte")
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Documento"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 15
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Apellido1"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 50
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)


            _oColumna = New DataColumn
            _oColumna.ColumnName = "Apellido2"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 50
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Nombre"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 50
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Género"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 1
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Pais"
            _oColumna.DataType = System.Type.GetType("System.Int32")
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Provincia"
            _oColumna.DataType = System.Type.GetType("System.Int32")
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Municipio"
            _oColumna.DataType = System.Type.GetType("System.Int32")
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Tipo_Via"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 2
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Número_Via"
            _oColumna.DataType = System.Type.GetType("System.Int32")
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Dirección"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 80
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Bloque"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 5
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Escalera"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 5
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Planta"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 5
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "Puerta"
            _oColumna.DataType = System.Type.GetType("System.String")
            _oColumna.MaxLength = 5
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)

            _oColumna = New DataColumn
            _oColumna.ColumnName = "CPostal"
            _oColumna.DataType = System.Type.GetType("System.Int32")
            _oBaseDatos.Tables("Comparecientes").Columns.Add(_oColumna)
        End Sub

        ''' <summary>
        ''' Crea la estructura de conceptos
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub Crea_Tabla_Concepto()
            Dim _oColumnas As DataColumn

            _oBaseDatos.Tables.Add("Conceptos")

            _oColumnas = New DataColumn
            _oColumnas.ColumnName = "ID"
            _oColumnas.DataType = System.Type.GetType("System.String")
            _oColumnas.MaxLength = 3
            _oBaseDatos.Tables("Conceptos").Columns.Add(_oColumnas)

            _oColumnas = New DataColumn
            _oColumnas.ColumnName = "Categoría"
            _oColumnas.DataType = System.Type.GetType("System.String")
            _oColumnas.MaxLength = 5
            _oBaseDatos.Tables("Conceptos").Columns.Add(_oColumnas)

            _oColumnas = New DataColumn
            _oColumnas.ColumnName = "Concepto"
            _oColumnas.DataType = System.Type.GetType("System.String")
            _oColumnas.MaxLength = 10
            _oBaseDatos.Tables("Conceptos").Columns.Add(_oColumnas)

            _oColumnas = New DataColumn
            _oColumnas.ColumnName = "Descripción"
            _oColumnas.DataType = System.Type.GetType("System.String")
            _oColumnas.MaxLength = 30
            _oBaseDatos.Tables("Conceptos").Columns.Add(_oColumnas)
        End Sub

        Private Sub Crea_Tabla_Protocolo()
            Dim _oColumnas As DataColumn

            _oBaseDatos.Tables.Add("Protocolo")

            _oColumnas = New DataColumn
            _oColumnas.ColumnName = "Año"
            _oColumnas.DataType = System.Type.GetType("System.UInt32")
            _oColumnas.DefaultValue = 0
            _oBaseDatos.Tables("Protocolo").Columns.Add(_oColumnas)

            _oColumnas = New DataColumn
            _oColumnas.ColumnName = "Número"
            _oColumnas.DataType = System.Type.GetType("System.UInt32")
            _oColumnas.DefaultValue = 0
            _oBaseDatos.Tables("Protocolo").Columns.Add(_oColumnas)

            _oColumnas = New DataColumn
            _oColumnas.ColumnName = "Compareciente"
            _oColumnas.DataType = System.Type.GetType("System.String")
            _oColumnas.DefaultValue = ""
            _oColumnas.MaxLength = 15
            _oBaseDatos.Tables("Protocolo").Columns.Add(_oColumnas)

            _oColumnas = New DataColumn
            _oColumnas.ColumnName = "Concepto"
            _oColumnas.DataType = System.Type.GetType("System.String")
            _oColumnas.DefaultValue = ""
            _oColumnas.MaxLength = 30
            _oBaseDatos.Tables("Protocolo").Columns.Add(_oColumnas)
        End Sub

        Public Function Grabar_Datos(ruta As String, nombre As String) As Boolean
            Dim _bResultado As Boolean = True

            Try
                _oBaseDatos.WriteXmlSchema(ruta & "\" & nombre & ".xsd")
                _oBaseDatos.WriteXml(ruta & "\" & nombre & ".xml")
            Catch ex As Exception
                _bResultado = False
            End Try
            Return _bResultado
        End Function

        Public Function Leer_Datos(ruta As String, nombre As String) As Boolean
            Dim _bResultado As Boolean = True

            Try
                _oBaseDatos.ReadXmlSchema(ruta & "\" & nombre & ".xsd")
                _oBaseDatos.ReadXml(ruta & "\" & nombre & ".xml")
            Catch ex As Exception
                _bResultado = False
            End Try
            Return _bResultado
        End Function

        Public Sub New()
            Crea_Tabla_Comparecientes()
            Crea_Tabla_Concepto()
            Crea_Tabla_Protocolo()
        End Sub
    End Class
End Namespace