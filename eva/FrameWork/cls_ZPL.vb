﻿Namespace FrameWork
    ''' <summary>
    ''' Traductor a Lenguaje a impresora de etiquetas Zebra
    ''' </summary>
    Public Class cls_ZPL

        ''' <summary>
        ''' Posiciona el puntero en la etiqueta.
        ''' </summary>
        ''' <param name="x">Posición X</param>
        ''' <param name="y">Posición Y</param>
        ''' <returns></returns>
        ''' <remarks>Las cordenadas se realizan en Dots.</remarks>
        Function Posición(ByVal x As Single, ByVal y As Single)
            Dim _sCadena

            _sCadena = "^FO" & x & "," & y
            Return _sCadena
        End Function

        ''' <summary>
        ''' Inicializa una nueva etiqueta.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function InicioEtiqueta() As String
            Return "^XA^CI0,21,36"
        End Function

        ''' <summary>
        ''' Finalización de una etiqueta.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function finalizaEtiqueta() As String
            Return "^XZ"
        End Function

        ''' <summary>
        ''' Define el ancho de una etiqueta.
        ''' </summary>
        ''' <param name="ancho">Definición de ancho de etiqueta.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function AnchoEtiqueta(ByVal ancho As Long) As String
            Return "^PW" & ancho
        End Function

        ''' <summary>
        ''' Imprime un cuadro de texto.
        ''' </summary>
        ''' <param name="x">Posición X</param>
        ''' <param name="y">Posición Y</param>
        ''' <param name="Talto">Alto de la fuente</param>
        ''' <param name="Tancho">Ancho de la fuente</param>
        ''' <param name="TipoFuente">Definición según la impresora, por defecto 0</param>
        ''' <param name="Rotación">Rotación del texto:
        ''' "N" => Normal
        ''' "L" => Izquierda"
        ''' </param>
        ''' <param name="CadenaDeTexto">Cadena de texto a imprimir.</param>
        ''' <param name="ancho">Ancho de la cadena de texto, necesario para alineación centrado y derecha, por defecto 100</param>
        ''' <param name="Alineación">Alineación del texto.
        ''' "L" => Izquierda.
        ''' "D" => Derecha.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function Texto(ByVal x As Single, ByVal y As Single, ByVal Talto As Integer, ByVal Tancho As Integer, Optional ByVal TipoFuente As Single = 0, Optional ByVal Rotación As String = "N", Optional ByVal CadenaDeTexto As String = "", Optional ByVal ancho As Long = 500, Optional ByVal Alineación As String = "L") As String
            Dim _sResultado As String = CadenaDeTexto

            _sResultado = _sResultado.Replace("ó", "\A2")
            _sResultado = _sResultado.Replace("Ó", "\A2")
            Return Posición(x, y) & "^A" & TipoFuente & Rotación & "," & Talto & "," & Tancho & "^FB" & ancho & ",,," & Alineación & "^FD" & _sResultado & "^FS"

        End Function

        ''' <summary>
        ''' Impresión de una caja.
        ''' </summary>
        ''' <param name="x">Posición X</param>
        ''' <param name="y">Posición Y</param>
        ''' <param name="ancho">Ancho de la caja</param>
        ''' <param name="alto">Alto de la caja.</param>
        ''' <param name="borde">Grosor del borde.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function Caja(ByVal x As Long, ByVal y As Long, ByVal ancho As Long, ByVal alto As Long, ByVal borde As Long) As String
            Return Posición(x, y) & "^GB" & ancho & "," & alto & "," & borde & "*^FS"
        End Function
    End Class
End Namespace