﻿Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Namespace AUBDF

    ''' <summary>
    ''' Gestiona los registros de una tabla específica o de una selección establecida.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class cls_Registros_mdb
        Inherits eva.AUBDF.cls_Conexión_mdb

        'Definición de variables internas.
        Private _dbAdaptador As OleDb.OleDbDataAdapter
        Private _dbTabla As New Data.DataSet
        Private _dbCopia As Data.DataTable
        Private _dbRegistro As Data.DataRow
        Private _sTabla As String = ""
        Private _sCampos_selección As String = "*"
        Private _sCampo_Clave As String = "ninguno"
        Private _sCampo_SubClave As String = ""
        Private _sCampo_Key As String = ""
        Private _sCampo_Orden As String = ""
        Private _nTipo_Campo_Clave As Byte = 0
        Private _nTipo_Campo_SubClave As Byte = 0
        Private _sValor_Clave As String = ""
        Private _sValor_SubClave As String = ""
        Private _ipuntero As Integer
        Private _sLinea_SQL As String = ""
        Private _bBúsquedaAprox As Boolean = False
        Private _bAlta As Boolean = False
        Private _bError_Campos As Boolean = False
        Private _bModificado As Boolean = False

        Private _oLog As New eva.FrameWork.cls_Log

        Public Function ConvertStr(valor As Object) As String
            Dim _sResultado As String
            Try
                _sResultado = valor.ToString
            Catch ex As Exception
                _sResultado = ""
                _oLog.Log("Error: Conversión a cadena: " & ex.Message)
            End Try

            Return _sResultado
        End Function

        Public Function ConvertSng(valor As Object) As Single
            Dim _nResultado As Single
            Try
                _nResultado = CSng(valor)
            Catch ex As Exception
                _nResultado = 0
                _oLog.Log("Error: Conversión a single: " & ex.Message)
            End Try

            Return _nResultado
        End Function

        Public Function ConvertInt(valor As Object) As Integer
            Dim _nResultado As Integer

            Try
                _nResultado = CInt(valor)
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function ConvertLng(valor As Object) As Long
            Dim _nResultado As Long

            Try
                _nResultado = CLng(valor)
            Catch ex As Exception
                _nResultado = 0
                _oLog.Log("Error: Conversión a long: " & ex.Message)
            End Try

            Return _nResultado
        End Function

        Public Function ConvertBool(valor As Object) As Boolean
            Dim _bResultado As Boolean

            Try
                _bResultado = CBool(valor)
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Conversión a bool: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        Public Function FormatoMoneda(valor As Single) As String
            Dim _oTmp As New eva.FrameWork.cls_UCF

            Return _oTmp.FormatoMoneda(valor.ToString)
        End Function

        Public Function FormatoDecimal(valor As Single) As String
            Dim _oTmp As New eva.FrameWork.cls_UCF

            Return _oTmp.FormatoDecimal(valor.ToString)
        End Function

        Public Property BúsquedaAprox As Boolean
            Get
                Return _bBúsquedaAprox
            End Get
            Set(ByVal value As Boolean)
                _bBúsquedaAprox = value
            End Set
        End Property

        Public Property LineaSQL As String
            Get
                Return _sLinea_SQL
            End Get
            Set(ByVal value As String)
                _sLinea_SQL = value
            End Set
        End Property

        Public ReadOnly Property est_Modificado As Boolean
            Get
                Return _bModificado
            End Get
        End Property

        ''' <summary>
        ''' Acceso directo al DataSet de la estructura.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DataOriginal() As DataSet
            Get
                Return _dbTabla
            End Get
            Set(ByVal value As DataSet)
                _dbTabla = value
            End Set
        End Property

        ''' <summary>
        ''' Acceso directo al dataviewer de la colección en memoria, para usar en formularios y componentes WPF
        ''' </summary>
        Public ReadOnly Property DatosView As DataView
            Get
                Return _dbTabla.Tables(0).DefaultView
            End Get
        End Property

        ''' <summary>
        ''' Acceso directo al columna viewer de la colección en memoria, para usar en formularios y componentes WPF
        ''' </summary>
        Public ReadOnly Property ColumnaView(Nombre_de_la_Columna As String) As Object
            Get
                Return _dbTabla.Tables(0).Columns(Nombre_de_la_Columna)
            End Get
        End Property

        ''' <summary>
        ''' Especifica los campos de la tabla que se seleccionarán.
        ''' </summary>
        ''' <value>*</value>
        ''' <returns>Relación de campos de la tabla separados con comas.</returns>
        ''' <remarks></remarks>
        Protected Property CamposSelección() As String
            Get
                Return _sCampos_selección
            End Get
            Set(ByVal value As String)
                _sCampos_selección = value
            End Set
        End Property

        Public Function MáximoValor(Optional campo As String = "") As ULong
            Dim _dbAdaptadorTemp As OleDb.OleDbDataAdapter
            Dim _dbTablaTemp As Data.DataSet
            Dim _sApertura As String
            Dim _nResultado As ULong = 0

            If campo = "" Then
                _sApertura = "SELECT max(" & Me.CampoClave & ") as maximo FROM " & Me.Tabla & ";"
            Else
                _sApertura = "SELECT max(" & campo & ") as maximo FROM " & Me.Tabla & ";"
            End If
            Try
                If BDDesconectado Then
                    Conectar()
                End If
                _sApertura = _sApertura
                _dbAdaptadorTemp = New OleDbDataAdapter(_sApertura, Conector)

                _dbTablaTemp = New Data.DataSet
                _dbAdaptadorTemp.Fill(_dbTablaTemp, _sTabla)

                _nResultado = CUInt(_dbTablaTemp.Tables(_sTabla).Rows(_ipuntero)("maximo"))
            Catch ex As Exception
                _nResultado = 0
                _oLog.Log("Error: Calculando máximo valor: " & ex.Message)
            End Try

            Return _nResultado
        End Function

        Public Function ÚltimoValor(Optional campo As String = "") As String
            Dim _dbAdaptadorTemp As OleDb.OleDbDataAdapter
            Dim _dbTablaTemp As Data.DataSet
            Dim _sApertura As String
            Dim _nResultado As String = ""

            If campo = "" Then
                _sApertura = "SELECT Last(" & Me.CampoClave & ") as maximo FROM " & Me.Tabla & ";"
            Else
                _sApertura = "SELECT Last(" & campo & ") as maximo FROM " & Me.Tabla & ";"
            End If

            Try
                If BDDesconectado Then
                    Conectar()
                End If
                _sApertura = _sApertura
                _dbAdaptadorTemp = New OleDbDataAdapter(_sApertura, Conector)

                _dbTablaTemp = New Data.DataSet
                _dbAdaptadorTemp.Fill(_dbTablaTemp, _sTabla)

                _nResultado = _dbTablaTemp.Tables(_sTabla).Rows(_ipuntero)("maximo").ToString
            Catch ex As Exception
                _nResultado = ""
                _oLog.Log("Error: Calculando último valor: " & ex.Message)
            End Try

            Return _nResultado
        End Function

        Public Function ÚltimoID(Optional campo As String = "") As Single
            Dim _oTemp As New eva.AUBDF.cls_Registros_mdb
            _oTemp.Conector = Me.Conector
            _oTemp.Tabla = "Temporal"

            Dim _nResultado As Single = 0

            If campo = "" Then
                _oTemp.LineaSQL = "SELECT MAX(" & Me.CampoClave & ") as maximo FROM " & Me.Tabla & ";"
            Else
                _oTemp.LineaSQL = "SELECT MAX(" & campo & ") as maximo FROM " & Me.Tabla & ";"
            End If

            Try
                _oTemp.Refrescar()
                _nResultado = CSng(_oTemp.Item("maximo"))
            Catch ex As Exception
                MsgBox(ex.Message)
                _nResultado = 0
                _oLog.Log("Error: Calculando último ID: " & ex.Message)
            End Try

            Return _nResultado
        End Function

        ''' <summary>
        ''' Nombre de la tabla de la base de datos que se accede.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Tabla() As String
            Get
                Return _sTabla
            End Get
            Set(ByVal value As String)
                _sTabla = value
            End Set
        End Property

        ''' <summary>
        ''' Especifica el campo clave primera que se indexará las búsquedas.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Nombre del campo</returns>
        ''' <remarks></remarks>
        Public Property CampoClave() As String
            Get
                Return _sCampo_Clave
            End Get
            Set(ByVal value As String)
                _sCampo_Clave = value
            End Set
        End Property

        ''' <summary>
        ''' Especifica el campo clave secundario que se indexarán las búsquedas.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Nombre del campo</returns>
        ''' <remarks></remarks>
        Public Property CampoSubClave() As String
            Get
                Return _sCampo_SubClave
            End Get
            Set(ByVal value As String)
                _sCampo_SubClave = value
            End Set
        End Property

        ''' <summary>
        ''' Especifica el campo clave de la base de datos de índice principal.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Nombre del campo</returns>
        ''' <remarks></remarks>
        Public Property CampoKey() As String
            Get
                Return _sCampo_Key
            End Get
            Set(ByVal value As String)
                _sCampo_Key = value
            End Set
        End Property

        ''' <summary>
        ''' Especifica el valor del campo clave para seleccionar el registro.
        ''' </summary>
        ''' <value></value>
        ''' <returns>valor indicado.</returns>
        ''' <remarks></remarks>
        Public Property ValorClave() As String
            Get
                Return _sValor_Clave
            End Get
            Set(ByVal value As String)
                If value = "*" Then
                    _sValor_Clave = value
                    Abrir()
                Else
                    If _sValor_Clave <> value Then
                        _sValor_Clave = value
                        Abrir()
                    Else
                        If Not BDConectado Then
                            Abrir()
                        End If
                    End If
                End If
            End Set
        End Property

        Public Property ValorSubClave() As String
            Get
                Return _sValor_SubClave
            End Get
            Set(ByVal value As String)
                If _sValor_SubClave <> value Then
                    _sValor_SubClave = value
                    Me.Abrir()
                Else
                    If Not BDConectado Then
                        Me.Abrir()
                    End If
                End If
            End Set
        End Property

        Public Property CampoOrden As String
            Get
                Return _sCampo_Orden
            End Get
            Set(value As String)
                _sCampo_Orden = value
            End Set
        End Property

        ''' <summary>
        ''' Devuelve la constante de difinición de tipo de datos que almacena el campo espeficiado.
        ''' </summary>
        ''' <value>Nombre del campo.</value>
        Private ReadOnly Property ItemTipo(ByVal campo As String) As Byte
            Get
                Dim _sResultado As Byte = 0

                If Not Me.BDConectado Then
                    Conectar()
                End If
                Try
                    Select Case _dbTabla.Tables(_sTabla).Columns.Item(campo).DataType.ToString
                        Case "System.String"
                            _sResultado = cTipoCadena
                        Case "System.Byte"
                            _sResultado = cTipoByte
                        Case "System.Int16"
                            _sResultado = cTipoEntero
                        Case "System.Int32"
                            _sResultado = cTipoEnteroLargo
                        Case "System.Boolean"
                            _sResultado = cTipoBoolean
                        Case "System.Single"
                            _sResultado = cTipoSingle
                        Case "System.Decimal"
                            _sResultado = cTipoDecimal
                        Case "System.Double"
                            _sResultado = cTipoDoble
                        Case "System.DateTime"
                            _sResultado = cTipoFecha
                        Case Else
                            _sResultado = cTipoSindefinir
                    End Select
                Catch ex As Exception
                    _sResultado = cTipoSindefinir
                    _oLog.Log("Error: Definición Tipo de un item de campo" & campo & ": " & ex.Message)
                End Try

                Return _sResultado
            End Get
        End Property

        Public Property Item(ByVal campo As String) As Object
            Get
                Dim _oUtil As New eva.FrameWork.cls_UCF
                Dim _oResultado As Object = Nothing

                Try
                    Select Case ItemTipo(campo)
                        Case cTipoBoolean
                            If Población > 0 Then _oResultado = _oUtil.NoNuloBol(_dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                        Case cTipoByte
                            If Población > 0 Then _oResultado = _oUtil.NoNuloByte(_dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                        Case cTipoCadena
                            If Población > 0 Then _oResultado = _oUtil.NoNuloStr(_dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                        Case cTipoEntero
                            If Población > 0 Then _oResultado = _oUtil.NoNuloInt(_dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                        Case cTipoEnteroLargo
                            If Población > 0 Then _oResultado = _oUtil.NoNuloUInt(_dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                        Case cTipoSingle
                            If Población > 0 Then _oResultado = _oUtil.NoNuloSng(_dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                        Case cTipoFecha
                            If Población > 0 Then _oResultado = _oUtil.NoNuloDat(_dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                        Case Else
                            If Población > 0 Then _oResultado = _oUtil.NoNuloStr(_dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                    End Select
                Catch ex As Exception
                    _oLog.Log("Error: Recogida de valor del campo " & campo & ": " & ex.Message)
                End Try

                Return _oResultado
            End Get
            Set(ByVal value As Object)
                Dim _oUtil As New eva.FrameWork.cls_UCF

                Try
                    If Not value Is Nothing Then
                        Select Case ItemTipo(campo)
                            Case cTipoBoolean
                                If Población > 0 Then _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloBol(value)
                            Case cTipoByte
                                If Población > 0 Then _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloByte(value)
                            Case cTipoCadena
                                If Población > 0 Then _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloStr(value)
                            Case cTipoEntero
                                If Población > 0 Then _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloInt(value)
                            Case cTipoEnteroLargo
                                If Población > 0 Then _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloUInt(value)
                            Case cTipoSingle
                                If Población > 0 Then _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloSng(value)
                            Case cTipoFecha
                                If Población > 0 Then _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloDat(value)
                            Case Else
                                If Población > 0 Then _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloStr(value)
                        End Select
                        _bModificado = True
                    End If
                Catch ex As Exception
                    _oLog.Log("Error: Almacenando valor al campo: " & campo & ": " & ex.Message)
                End Try
                
            End Set
        End Property

        Public ReadOnly Property ItemEsTexto(ByVal campo As String) As Boolean
            Get
                Dim _bResultado As Boolean = False

                If ItemTipo(campo) = cTipoCadena Then
                    _bResultado = True
                End If

                Return _bResultado
            End Get
        End Property

        Public ReadOnly Property ItemEsNumérico(campo As String) As Boolean
            Get
                Dim _bResultado As Boolean = False

                If ItemTipo(campo) = cTipoByte Or ItemTipo(campo) = cTipoEntero Or ItemTipo(campo) = cTipoEnteroLargo Or ItemTipo(campo) = cTipoSingle Or ItemTipo(campo) = cTipoDecimal Then
                    _bResultado = True
                End If

                Return _bResultado
            End Get
        End Property

        Public ReadOnly Property ItemEsFecha(campo As String) As Boolean
            Get
                Dim _bResultado As Boolean = False

                If ItemTipo(campo) = cTipoFecha Then
                    _bResultado = True
                End If

                Return _bResultado
            End Get
        End Property

        Public ReadOnly Property ItemEsBooleano(campo As String) As Boolean
            Get
                Dim _bResultado As Boolean = False

                If ItemTipo(campo) = cTipoBoolean Then
                    _bResultado = True
                End If

                Return _bResultado
            End Get
        End Property

        Protected ReadOnly Property ItemObj(ByVal campo As String) As Object
            Get
                Return _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo)
            End Get
        End Property

        Protected ReadOnly Property ItemObj(ByVal campo As Integer) As Object
            Get
                Return _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo)
            End Get
        End Property

        Public Property Puntero() As Integer
            Get
                Return _ipuntero
            End Get
            Set(ByVal value As Integer)
                _ipuntero = value
            End Set
        End Property

        Public ReadOnly Property Población() As Integer
            Get
                Try
                    Return _dbTabla.Tables(_sTabla).Rows.Count
                Catch ex As Exception
                    Return 0
                End Try

            End Get
        End Property

        Public Function Deshacer(campo As String) As Boolean
            Dim _bResultado As Boolean = True

            Try
                _dbTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _dbCopia(_ipuntero)(campo)
            Catch ex As Exception
                _bResultado = False
            End Try

            Return _bResultado
        End Function

        Public Sub Refrescar()
            Abrir()
        End Sub

        Private Sub Abrir()
            Dim _sApertura As String = ""

            If BDDesconectado Then
                Conectar()
            End If

            If BDConectado Then
                If _sLinea_SQL = "" Then
                    Try
                        _sApertura = "Select " & _sCampos_selección & " from " & _sTabla
                        If _sValor_Clave <> "" And _sValor_Clave <> "*" Then
                            If TipoDatos(Tabla, _sCampo_Clave) = cTipoCadena Then
                                If _bBúsquedaAprox Then
                                    _sApertura = _sApertura & " WHERE [" & _sCampo_Clave & "] like '%" & _sValor_Clave & "%' "
                                Else
                                    _sApertura = _sApertura & " WHERE [" & _sCampo_Clave & "] = '" & _sValor_Clave & "' "
                                End If
                            Else
                                _sApertura = _sApertura & " WHERE [" & _sCampo_Clave & "] = " & _sValor_Clave & " "
                            End If

                            If _sValor_SubClave <> "" Then
                                If TipoDatos(Tabla, _sCampo_SubClave) = cTipoCadena Then
                                    _sApertura = _sApertura & " AND [" & _sCampo_SubClave & "] = '" & _sValor_SubClave & "' "
                                Else
                                    _sApertura = _sApertura & " AND [" & _sCampo_SubClave & "] = " & _sValor_SubClave & " "
                                End If
                            End If
                        End If

                        If _sCampo_Orden <> "" Then
                            _sApertura = _sApertura & " ORDER BY [" & _sCampo_Orden & "]"
                        End If


                        _dbAdaptador = New OleDbDataAdapter(_sApertura & ";", Conector)

                        _dbTabla = New Data.DataSet
                        _dbAdaptador.Fill(_dbTabla, _sTabla)
                        Me.Puntero = 0
                        _dbCopia = _dbTabla.Tables(_sTabla).Copy
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Critical, "ERROR: Acceso a los datos.")
                        _oLog.Log("Error: Apertura de datos: " & ex.Message)
                    End Try
                Else
                    Try
                        _sApertura = _sLinea_SQL
                        _dbAdaptador = New OleDbDataAdapter(_sApertura, Conector)

                        _dbTabla = New Data.DataSet
                        _dbAdaptador.Fill(_dbTabla, _sTabla)
                        Me.Puntero = 0
                        Me._bModificado = False
                    Catch ex As Exception
                        MsgBox(ex.Message.ToString, MsgBoxStyle.Critical, "EVolution Apps")
                        _oLog.Log("Error: Apertura de datos: " & ex.Message)
                    End Try
                End If
            End If
        End Sub

        ''' <summary>
        ''' Inicio de los registros seleccionados.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function RegInicio() As Boolean
            Dim _bResultado As Boolean

            If Me.Puntero = 0 Then
                _bResultado = True
            Else
                _bResultado = False
            End If

            Return _bResultado
        End Function

        ''' <summary>
        ''' Final de registros seleccionados.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function RegFinal() As Boolean
            Dim _bResultado As Boolean

            If Me.Puntero >= Me.Población Then
                _bResultado = True
            Else
                _bResultado = False
            End If

            Return _bResultado
        End Function

        Public Sub RegSiguiente()
            If Me.Puntero < Me.Población Then
                Me.Puntero = Me.Puntero + 1
            End If
        End Sub

        Public Sub RegAnterior()
            If Me.Puntero > 0 Then
                Me.Puntero = Me.Puntero - 1
            End If
        End Sub

        ''' <summary>
        ''' Actualiza los cambios realizados en la tabla.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub ActualizarRegistro()
            Dim _sCadena As String = ""
            Dim _nPuntero As Integer = 0

            While _nPuntero < _dbTabla.Tables(_sTabla).Columns.Count
                If _dbTabla.Tables(_sTabla).Rows(0)(_nPuntero).ToString <> "" Then
                    If _sCadena & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName.ToString <> Me.CampoClave And _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName.ToString <> Me.CampoKey Then
                        Select Case _dbTabla.Tables(_sTabla).Columns(_nPuntero).DataType.Name
                            Case "String"
                                _sCadena = _sCadena & "[" & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "]" & "='" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & "', "
                            Case "Int32"
                                _sCadena = _sCadena & "[" & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "]" & "=" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & ", "
                            Case "Int16"
                                _sCadena = _sCadena & "[" & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "]" & "=" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & ", "
                            Case "Double"
                                _sCadena = _sCadena & "[" & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "]" & "=" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & ", "
                            Case "Byte"
                                _sCadena = _sCadena & "[" & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "]" & "=" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & ", "
                            Case "Boolean"
                                If _dbTabla.Tables(_sTabla).Rows(0)(_nPuntero).ToString = "True" Then
                                    _sCadena = _sCadena & "[" & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "]" & "=True, "
                                Else
                                    _sCadena = _sCadena & "[" & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "]" & "=False, "
                                End If
                            Case "DateTime"
                                _sCadena = _sCadena & "[" & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "]" & "='" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & "', "
                            Case Else
                                _sCadena = _sCadena & "[" & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "]" & "=" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString.Replace(",", ".") & ", "
                        End Select
                    End If
                End If
                _nPuntero = _nPuntero + 1
            End While

            If _sCadena <> "" Then
                _sCadena = "UPDATE " & Me.Tabla & " SET " & _sCadena
                _sCadena = _sCadena.Substring(0, _sCadena.Length - 2)
                If Me.CampoClave <> "ninguno" Then
                    If Me.ValorClave <> "" Then
                        If ItemEsNumérico(CampoClave) Then
                            _sCadena = _sCadena & " WHERE [" & Me.CampoClave & "]=" & Me.ValorClave & ""
                        Else
                            _sCadena = _sCadena & " WHERE [" & Me.CampoClave & "]='" & Me.ValorClave & "'"
                        End If
                    Else
                        If ItemEsNumérico(CampoClave) Then
                            _sCadena = _sCadena & " WHERE [" & Me.CampoClave & "]=" & Me.Item(Me.CampoClave).ToString
                        Else
                            _sCadena = _sCadena & " WHERE [" & Me.CampoClave & "]='" & Me.Item(Me.CampoClave).ToString & "'"
                        End If
                    End If
                End If
            End If

            Me.SQLExec(_sCadena & ";")
            _bAlta = False
        End Sub

        Public Sub Nuevo_registro()
            If IsNothing(_dbTabla.Tables(_sTabla)) Then
                _dbTabla.Tables.Add(_sTabla)
            End If

            _dbTabla.Tables(_sTabla).Rows.Add()
            _bAlta = True
        End Sub

        Public Sub Actualizar()
            If _bAlta Then
                Añadir_registro()
            Else
                ActualizarRegistro()
            End If
        End Sub

        ''' <summary>
        ''' Añade un nuevo registro a la tabla.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub Añadir_registro()
            Dim _sCadena_campos As String = ""
            Dim _sCadena_valores As String = ""
            Dim _sCadena As String = ""
            Dim _nPuntero As Integer = 0

            While _nPuntero < _dbTabla.Tables(0).Columns.Count
                If _dbTabla.Tables(_sTabla).Rows(0)(_nPuntero).ToString <> "" Then
                    If _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName.ToString <> Me.CampoKey Then
                        Select Case _dbTabla.Tables(_sTabla).Columns(_nPuntero).DataType.Name
                            Case "String"
                                _sCadena_campos = _sCadena_campos & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & ", "
                                _sCadena_valores = _sCadena_valores & "'" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & "', "
                            Case "Int32"
                                _sCadena_campos = _sCadena_campos & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & ", "
                                _sCadena_valores = _sCadena_valores & "" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & ", "
                            Case "Int16"
                                _sCadena_campos = _sCadena_campos & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & ", "
                                _sCadena_valores = _sCadena_valores & "" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & ", "
                            Case "Single"
                                _sCadena_campos = _sCadena_campos & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & ", "
                                _sCadena_valores = _sCadena_valores & "" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString.Replace(",", ".") & ", "
                            Case "Double"
                                _sCadena_campos = _sCadena_campos & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & ", "
                                _sCadena_valores = _sCadena_valores & "" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString.Replace(",", ".") & ", "
                            Case "Byte"
                                _sCadena_campos = _sCadena_campos & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & ", "
                                _sCadena_valores = _sCadena_valores & "" & Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString & ", "
                            Case "Boolean"
                                _sCadena_campos = _sCadena_campos & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & ", "
                                If Item(_dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName).ToString = "True" Then
                                    _sCadena_valores = _sCadena_valores & "1, "
                                Else
                                    _sCadena_valores = _sCadena_valores & "0, "
                                End If
                            Case "DateTime"
                                _sCadena_campos = _sCadena_campos & _dbTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & ", "
                                _sCadena_valores = _sCadena_valores & "'" & _dbTabla.Tables(_sTabla).Rows(0)(_nPuntero).ToString & "', "
                        End Select
                    End If
                End If
                _nPuntero = _nPuntero + 1
            End While

            If _sCadena_campos <> "" Then
                _sCadena_campos = _sCadena_campos.Substring(0, _sCadena_campos.Length - 2)
                _sCadena_valores = _sCadena_valores.Substring(0, _sCadena_valores.Length - 2)

                _sCadena = "INSERT INTO " & Me.Tabla & " (" & _sCadena_campos & ") VALUES (" & _sCadena_valores & ")"

                Me.SQLExec(_sCadena)
            End If
            _bAlta = False
        End Sub

        Public Sub Borrar()
            Dim _sCadena As String = ""

            If ValorClave = "*" Then
                _sCadena = "DELETE " & Tabla & ".* FROM [" & Me.Tabla & "];"
            Else
                If Me.CampoClave <> "" And Me.ValorClave <> "" Then
                    If ItemEsTexto(CampoClave) Then
                        _sCadena = "DELETE FROM " & Me.Tabla & " WHERE " & CampoClave & " = '" & ValorClave & "' ;"
                    Else
                        _sCadena = "DELETE FROM " & Me.Tabla & " WHERE " & CampoClave & " = " & ValorClave & " ;"
                    End If

                End If
            End If
            Me.SQLExec(_sCadena)
        End Sub

        ''' <summary>
        ''' Constructor de la clase.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()

        End Sub
    End Class

End Namespace