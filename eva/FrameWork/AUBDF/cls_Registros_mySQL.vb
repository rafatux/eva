﻿Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports MySql.Data.MySqlClient
Imports MySql.Data.Types

Namespace AUBDF

    ''' <summary>
    ''' Gestiona los registros de una tabla específica o de una selección establecida.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class cls_Registros_mySQL
        Inherits eva.AUBDF.cls_Conexión_mySQL

        'Definición de variables internas.
        Private _oTabla As New Data.DataSet
        Private _oCopia As Data.DataTable
        Private _oRegistro As Data.DataRow
        Private _sTabla As String = ""
        Private _sCampos_selección As String = "*"
        Private _sCampo_Clave As String = "ninguno"
        Private _sCampo_SubClave As String = ""
        Private _sCampo_Key As String = ""
        Private _sCampo_Orden As String = ""
        Private _nTipo_Campo_Clave As Byte = 0
        Private _nTipo_Campo_SubClave As Byte = 0
        Private _sValor_Clave As String = ""
        Private _sValor_SubClave As String = ""
        Private _ipuntero As Integer
        Private _sLinea_SQL As String = ""
        Private _bBúsquedaAprox As Boolean = False
        Private _bAlta As Boolean = False
        Private _bError_Campos As Boolean = False
        Private _bModificado As Boolean = False

        Public Property BúsquedaAprox As Boolean
            Get
                Return _bBúsquedaAprox
            End Get
            Set(ByVal value As Boolean)
                _bBúsquedaAprox = value
            End Set
        End Property

        Public Property LineaSQL As String
            Get
                Return _sLinea_SQL
            End Get
            Set(ByVal value As String)
                _sLinea_SQL = value
            End Set
        End Property

        Public Property DataOriginal() As DataSet
            Get
                Return _oTabla
            End Get
            Set(ByVal value As DataSet)
                _oTabla = value
            End Set
        End Property

        Public ReadOnly Property DatosView As DataView
            Get
                Return _oTabla.Tables(0).DefaultView
            End Get
        End Property

        Public ReadOnly Property ColumnaView(Nombre_de_la_Columna As String) As Object
            Get
                Return _oTabla.Tables(0).Columns(Nombre_de_la_Columna)
            End Get
        End Property

        Protected Property CamposSelección() As String
            Get
                Return _sCampos_selección
            End Get
            Set(ByVal value As String)
                _sCampos_selección = value
            End Set
        End Property

        Public Function MáximoValor(Optional campo As String = "") As ULong
            Dim _oAdaptador As MySql.Data.MySqlClient.MySqlDataAdapter
            Dim _oTablaTemp As New Data.DataSet
            Dim _nResultado As ULong = 0

            If campo = "" Then
                _oAdaptador = New MySql.Data.MySqlClient.MySqlDataAdapter("SELECT max(" & Me.CampoClave & ") as maximo FROM " & Me.Tabla & ";", Conector)
            Else
                _oAdaptador = New MySql.Data.MySqlClient.MySqlDataAdapter("SELECT max(" & campo & ") as maximo FROM " & Me.Tabla & ";", Conector)
            End If
            Try
                _oAdaptador.Fill(_oTablaTemp, Me.Tabla)
                _nResultado = CUInt(_oTablaTemp.Tables(0).Rows(0)("maximo"))
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function ÚltimoValor(Optional campo As String = "") As String
            Dim _oAdaptador As MySql.Data.MySqlClient.MySqlDataAdapter
            Dim _oTablaTemp As New Data.DataSet
            Dim _nResultado As String = ""

            If campo = "" Then
                _oAdaptador = New MySql.Data.MySqlClient.MySqlDataAdapter("SELECT Last(" & Me.CampoClave & ") as maximo FROM " & Me.Tabla & ";", Conector)
            Else
                _oAdaptador = New MySql.Data.MySqlClient.MySqlDataAdapter("SELECT Last(" & campo & ") as maximo FROM " & Me.Tabla & ";", Conector)
            End If

            Try
                _oAdaptador.Fill(_oTablaTemp, Me.Tabla)
                _nResultado = _oTablaTemp.Tables(_sTabla).Rows(_ipuntero)("maximo").ToString
            Catch ex As Exception
                _nResultado = ""
            End Try

            Return _nResultado
        End Function

        Public Function ÚltimoID(Optional campo As String = "") As Single
            Dim _oTemp As New eva.AUBDF.cls_Registros_mySQL
            Dim _nResultado As Single = 0

            _oTemp.Conector = Me.Conector
            _oTemp.Tabla = "Temporal"
            If campo = "" Then
                _oTemp.LineaSQL = "SELECT MAX(" & Me.CampoClave & ") as maximo FROM " & Me.Tabla & ";"
            Else
                _oTemp.LineaSQL = "SELECT MAX(" & campo & ") as maximo FROM " & Me.Tabla & ";"
            End If

            Try
                _oTemp.Abrir()
                _nResultado = CSng(_oTemp.Item("maximo"))
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Property Tabla() As String
            Get
                Return _sTabla
            End Get
            Set(ByVal value As String)
                _sTabla = value
                LineaSQL = "Select * FROM " & Tabla & " LIMIT 1"
                Abrir()
                LineaSQL = ""
            End Set
        End Property

        Public Property CampoClave() As String
            Get
                Return _sCampo_Clave
            End Get
            Set(ByVal value As String)
                _sCampo_Clave = value
            End Set
        End Property

        Public Property CampoSubClave() As String
            Get
                Return _sCampo_SubClave
            End Get
            Set(ByVal value As String)
                _sCampo_SubClave = value
            End Set
        End Property

        Public Property CampoKey() As String
            Get
                Return _sCampo_Key
            End Get
            Set(ByVal value As String)
                _sCampo_Key = value
            End Set
        End Property

        Public Property ValorClave() As String
            Get
                Return _sValor_Clave
            End Get
            Set(ByVal value As String)
                If value = "*" Then
                    _sValor_Clave = ""
                    Abrir()
                Else
                    If _sValor_Clave <> value Then
                        _sValor_Clave = value
                        Abrir()
                    End If
                End If
            End Set
        End Property

        Public Property ValorSubClave() As String
            Get
                Return _sValor_SubClave
            End Get
            Set(ByVal value As String)
                If _sValor_SubClave <> value Then
                    _sValor_SubClave = value
                    Me.Abrir()
                End If
            End Set
        End Property

        Public Property CampoOrden As String
            Get
                Return _sCampo_Orden
            End Get
            Set(value As String)
                _sCampo_Orden = value
            End Set
        End Property

        Public Property Item(ByVal campo As String) As Object
            Get
                Dim _oUtil As New eva.FrameWork.cls_UCF
                Dim _oResultado As Object = Nothing

                Try
                    Select Case _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo).GetType.Name
                        Case "Boolean"
                            If Población > 0 Then
                                _oResultado = _oUtil.NoNuloBol(_oTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                            Else
                                _oResultado = False
                            End If

                        Case "Byte"
                            If Población > 0 Then
                                _oResultado = _oUtil.NoNuloByte(_oTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                            Else
                                _oResultado = 0
                            End If
                        Case "String"
                            If Población > 0 Then
                                _oResultado = _oUtil.NoNuloStr(_oTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                            Else
                                _oResultado = ""
                            End If
                        Case "Int16"
                            If Población > 0 Then
                                _oResultado = _oUtil.NoNuloInt(_oTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                            Else
                                _oResultado = 0
                            End If
                        Case "Int32"
                            If Población > 0 Then
                                _oResultado = _oUtil.NoNuloUInt(_oTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                            Else
                                _oResultado = 0
                            End If
                        Case "Single", "Double", "Decimal"
                            If Población > 0 Then
                                _oResultado = _oUtil.NoNuloSng(_oTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                            Else
                                _oResultado = 0
                            End If
                        Case "DateTime"
                            If Población > 0 Then
                                _oResultado = _oUtil.NoNuloDat(_oTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                            Else
                                _oResultado = _oUtil.NoNuloDat("01/01/1900")
                            End If
                        Case Else
                            If Población > 0 Then
                                _oResultado = _oUtil.NoNuloStr(_oTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                            Else
                                _oResultado = ""
                            End If
                    End Select
                Catch ex As Exception
                    _oResultado = _oUtil.NoNuloStr(_oTabla.Tables(_sTabla).Rows(_ipuntero)(campo))
                End Try

                Return _oResultado
            End Get
            Set(ByVal value As Object)
                Dim _oUtil As New eva.FrameWork.cls_UCF

                If Not value Is Nothing Then
                    Select Case _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo).GetType.Name
                        Case "Boolean"
                            If Población > 0 Then _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloBol(value)
                        Case "Byte"
                            If Población > 0 Then _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloByte(value)
                        Case "String"
                            If Población > 0 Then _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloStr(value)
                        Case "Int16"
                            If Población > 0 Then _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloInt(value)
                        Case "Int32"
                            If Población > 0 Then _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloUInt(value)
                        Case "Single", "Double"
                            If Población > 0 Then _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloSng(value)
                        Case "DateTime"
                            If Población > 0 Then _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloDat(value)
                        Case Else
                            If Población > 0 Then _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oUtil.NoNuloStr(value)
                    End Select
                    _bModificado = True
                End If
            End Set
        End Property

        Protected ReadOnly Property ItemObj(ByVal campo As String) As Object
            Get
                Return _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo)
            End Get
        End Property

        Protected ReadOnly Property ItemObj(ByVal campo As Integer) As Object
            Get
                Return _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo)
            End Get
        End Property

        Public Property Puntero() As Integer
            Get
                Return _ipuntero
            End Get
            Set(ByVal value As Integer)
                _ipuntero = value
            End Set
        End Property

        Public ReadOnly Property Población() As Integer
            Get
                Try
                    Return _oTabla.Tables(_sTabla).Rows.Count
                Catch ex As Exception
                    Return 0
                End Try

            End Get
        End Property

        Public Function Deshacer(campo As String) As Boolean
            Dim _bResultado As Boolean = True

            Try
                _oTabla.Tables(_sTabla).Rows(_ipuntero)(campo) = _oCopia(_ipuntero)(campo)
            Catch ex As Exception
                _bResultado = False
            End Try

            Return _bResultado
        End Function

        Public Sub Refrescar()
            Abrir()
        End Sub

        Private Function Abrir() As Boolean
            Dim _oAdaptador As MySql.Data.MySqlClient.MySqlDataAdapter
            Dim _sApertura As String = ""
            Dim _bResultado As Boolean = False

            If Conector.State = ConnectionState.Open Then
                If _sLinea_SQL <> "" Then
                    _sApertura = _sLinea_SQL
                Else
                    _sApertura = "SELECT * FROM " & _sTabla
                    If _sValor_Clave <> "" Then
                        If _oTabla.Tables(_sTabla).Rows(_ipuntero)(_sCampo_Clave).GetType.Name = "String" Then
                            If _bBúsquedaAprox Then
                                _sApertura = _sApertura & " WHERE " & _sCampo_Clave & " like '%" & _sValor_Clave & "%' "
                            Else
                                _sApertura = _sApertura & " WHERE " & _sCampo_Clave & " = '" & _sValor_Clave & "' "
                            End If
                        Else
                            _sApertura = _sApertura & " WHERE " & _sCampo_Clave & " = " & _sValor_Clave & " "
                        End If
                        If _sValor_SubClave <> "" Then
                            If _oTabla.Tables(_sTabla).Rows(_ipuntero)(_sCampo_SubClave).GetType.Name = "String" Then
                                _sApertura = _sApertura & " AND " & _sCampo_SubClave & " = '" & _sValor_SubClave & "' "
                            Else
                                _sApertura = _sApertura & " AND " & _sCampo_SubClave & " = " & _sValor_SubClave & " "
                            End If
                        End If
                    End If
                    If _sCampo_Orden <> "" Then
                        _sApertura = _sApertura & " ORDER BY " & _sCampo_Clave
                    End If
                End If

                Try
                    _oTabla = Nothing
                    _oTabla = New DataSet
                    _oAdaptador = New MySql.Data.MySqlClient.MySqlDataAdapter(_sApertura & ";", Conector)
                    _oAdaptador.Fill(_oTabla, _sTabla)

                    Me.Puntero = 0
                    _oCopia = _oTabla.Tables(_sTabla).Copy

                    _bResultado = True
                Catch ex As Exception
                    _bResultado = False
                End Try
            End If

            Return _bResultado
        End Function

        Public Sub Nuevo_registro()
            If IsNothing(_oTabla.Tables(_sTabla)) Then
                _oTabla.Tables.Add(_sTabla)
            End If

            _oTabla.Tables(_sTabla).Rows.Add()
            Puntero = Población - 1
            _bAlta = True
        End Sub

        Public Sub Actualizar()
            If _bAlta Then
                Añadir_registro()
            Else
                ActualizarRegistro()
            End If
        End Sub

        Private Sub ActualizarRegistro()
            Dim _oUtil As New eva.FrameWork.cls_UCF
            Dim _sCadena As String = ""
            Dim _nPuntero As Integer = 0

            While _nPuntero < _oTabla.Tables(_sTabla).Columns.Count
                If _oTabla.Tables(_sTabla).Rows(0)(_nPuntero).ToString <> "" Then
                    If _sCadena & _oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName.ToString <> Me.CampoClave And _oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName.ToString <> Me.CampoKey Then
                        _sCadena = _sCadena & _oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & "="
                        Select Case _oTabla.Tables(_sTabla).Columns(_nPuntero).DataType.Name
                            Case "String"
                                _sCadena = _sCadena & _oUtil.SQL_Cadena(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                            Case "Byte", "Int32", "Int16", "Double", "Single", "Decimal"
                                _sCadena = _sCadena & _oUtil.SQL_Número(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                            Case "Boolean"
                                _sCadena = _sCadena & _oUtil.SQL_Booleano(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                            Case "DateTime"
                                _sCadena = _sCadena & _oUtil.SQL_Fecha(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                            Case Else
                                _sCadena = _sCadena & _oUtil.SQL_Cadena(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                        End Select
                    End If
                End If
                _nPuntero = _nPuntero + 1
            End While

            If _sCadena <> "" Then
                _sCadena = _sCadena.Substring(0, _sCadena.Length - 2)
                _sCadena = "UPDATE " & Me.Tabla & " SET " & _sCadena
                If Me.CampoClave <> "" Then
                    If _oTabla.Tables(_sTabla).Columns(Me.CampoClave).DataType.Name = "String" Then
                        _sCadena = _sCadena & " WHERE " & Me.CampoClave & "='" & Me.ValorClave & "'"
                    Else
                        _sCadena = _sCadena & " WHERE " & Me.CampoClave & "=" & Me.ValorClave & ""
                    End If
                End If
            End If

            Me.SQLExec(_sCadena & ";")
            _bAlta = False
            _bModificado = False
        End Sub

        Private Sub Añadir_registro()
            Dim _oUtil As New eva.FrameWork.cls_UCF
            Dim _sCadena As String = ""
            Dim _nPuntero As Integer = 0
            Dim _sCadena_campos As String = ""
            Dim _sCadena_valores As String = ""

            While _nPuntero < _oTabla.Tables(_sTabla).Columns.Count
                If _oTabla.Tables(_sTabla).Rows(0)(_nPuntero).ToString <> "" Then
                    If _oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName.ToString <> Me.CampoKey Then
                        _sCadena_campos = _sCadena_campos & _oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName & ", "
                        Select Case _oTabla.Tables(_sTabla).Columns(_nPuntero).DataType.Name
                            Case "String"
                                _sCadena_valores = _sCadena_valores & _oUtil.SQL_Cadena(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                            Case "Byte", "Int32", "Int16", "Int64", "Decimal", "Double", "Single", "Decimal"
                                _sCadena_valores = _sCadena_valores & _oUtil.SQL_Número(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                            Case "Boolean"
                                _sCadena_valores = _sCadena_valores & _oUtil.SQL_Booleano(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                            Case "DateTime"
                                _sCadena_valores = _sCadena_valores & _oUtil.SQL_Fecha(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                            Case Else
                                _sCadena_valores = _sCadena_valores & _oUtil.SQL_Cadena(Item(_oTabla.Tables(_sTabla).Columns(_nPuntero).ColumnName)) & ", "
                        End Select
                    End If
                End If
                _nPuntero = _nPuntero + 1
            End While

            If _sCadena_campos <> "" Then
                _sCadena_campos = _sCadena_campos.Substring(0, _sCadena_campos.Length - 2)
                _sCadena_valores = _sCadena_valores.Substring(0, _sCadena_valores.Length - 2)
                _sCadena = "INSERT INTO " & Me.Tabla & " (" & _sCadena_campos & ") VALUES (" & _sCadena_valores & ")"
                Me.SQLExec(_sCadena)
            End If
            _bAlta = False
            _bModificado = False
        End Sub

        Public Function Borrar() As Boolean
            Dim _oUtil As New eva.FrameWork.cls_UCF
            Dim _sCadena As String = ""

            If ValorClave = "*" Then
                _sCadena = "DELETE " & Tabla & ".* FROM [" & Me.Tabla & "];"
            Else
                If Me.CampoClave <> "" And Me.ValorClave <> "" Then
                    If _oTabla.Tables(_sTabla).Rows(_ipuntero)(_sCampo_Clave).GetType.Name = "String" Then
                        _sCadena = "DELETE FROM " & Me.Tabla & " WHERE " & CampoClave & " = " & _oUtil.SQL_Cadena(ValorClave) & ";"
                    Else
                        _sCadena = "DELETE FROM " & Me.Tabla & " WHERE " & CampoClave & " = " & ValorClave & " ;"
                    End If
                End If
            End If

            Return Me.SQLExec(_sCadena)
        End Function

        ''' <summary>
        ''' Constructor de la clase.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()

        End Sub
    End Class

End Namespace