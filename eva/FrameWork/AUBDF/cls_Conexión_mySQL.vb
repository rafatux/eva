﻿Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports MySql.Data.MySqlClient
Imports MySql.Data.Types


Namespace AUBDF
    Public Class cls_Conexión_mySQL
        Private _oConexión As New MySql.Data.MySqlClient.MySqlConnection
        Private _oComando As New MySql.Data.MySqlClient.MySqlCommand

        Private _sServidor As String = ""
        Private _sUsuario As String = ""
        Private _sPassword As String = ""
        Private _sBase_de_datos As String = ""


        Public Property Usuario As String
            Get
                Return _sUsuario
            End Get
            Set(value As String)
                _sUsuario = value
            End Set
        End Property

        Public Property Password As String
            Get
                Return _sPassword
            End Get
            Set(value As String)
                _sPassword = value
            End Set
        End Property

        Public Property Servidor As String
            Get
                Return _sServidor
            End Get
            Set(value As String)
                _sServidor = value
            End Set
        End Property

        Public Property BaseDeDatos As String
            Get
                Return _sBase_de_datos
            End Get
            Set(value As String)
                _sBase_de_datos = value
            End Set
        End Property


        Public ReadOnly Property CadenaConexión() As String
            Get
                '_oConfigXML.cmp_RUTA_principal_BD = "Server=172.26.0.200; user id=root; password=Sitel7010; database=mercanet"
                Return "Server=" & _sServidor & "; user id=" & _sUsuario & ";password=" & _sPassword & "; database=" & _sBase_de_datos
            End Get
        End Property

        Public Property Conector() As MySql.Data.MySqlClient.MySqlConnection
            Get
                Return _oConexión
            End Get
            Set(ByVal value As MySql.Data.MySqlClient.MySqlConnection)
                _oConexión = value
            End Set
        End Property

        Public Function Conectar() As Boolean
            Dim _bResultado As Boolean = False

            Try

                If _oConexión.State = ConnectionState.Closed Then
                    If _oConexión.State = ConnectionState.Broken Then
                        _bResultado = False
                    Else
                        _oConexión.ConnectionString = Me.CadenaConexión
                        _oConexión.Open()
                        _bResultado = True
                    End If
                Else
                    _bResultado = True
                End If

            Catch ex As Exception
                _bResultado = False
            End Try

            Return _bResultado
        End Function

        Public Function Esta_Conectado() As Boolean
            Dim _bResultado As Boolean = False

            If _oConexión.State <> ConnectionState.Closed Then _bResultado = True

            Return _bResultado
        End Function

        Public Function Desconectar() As Boolean
            Dim _bResultado As Boolean = False

            Try
                If _oConexión.State = ConnectionState.Open Then
                    _oConexión.Close()
                    _bResultado = True
                Else
                    _bResultado = False
                End If
            Catch ex As Exception
                _bResultado = False
            End Try

            Return _bResultado
        End Function

        Public Function SQLExec(ByVal Orden As String) As Boolean

            Dim _bResultado As Boolean = False

            Try
                If Orden <> "" Then
                    If Not Esta_Conectado() Then Conectar()
                    _oComando.CommandText = Orden
                    _oComando.Connection = _oConexión
                    _oComando.CommandType = CommandType.Text
                    _oComando.ExecuteNonQuery()
                    _bResultado = True
                Else
                    _bResultado = False
                End If
            Catch ex As Exception
                _bResultado = False
            End Try

            Return _bResultado
        End Function

        Public Sub New()

        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class
End Namespace