﻿Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Xml


Namespace AUBDF
    ''' <summary>
    ''' Gestiona las conexiones a las base de datos.
    ''' </summary>
    ''' <remarks>Base de datos soportadas:
    ''' .mdb
    ''' .ACCDB
    ''' </remarks>
    Public Class cls_Conexión_mdb
        Private _oLog As New eva.FrameWork.cls_Log

        ''' <summary>
        ''' Constante cuando no se ha podido definir el tipo de columna.
        ''' </summary>
        ''' Definición de tipo de campo sin definir
        Friend Const cTipoSindefinir As Byte = 0
        ''' <summary>
        ''' Constante de tipo de campo cadena
        ''' </summary>
        Friend Const cTipoCadena As Byte = 1
        ''' <summary>
        ''' Constante de tipo de campo bytes
        ''' </summary>
        Friend Const cTipoByte As Byte = 2
        ''' <summary>
        ''' Constante de dtipo de campo entero
        ''' </summary>
        Friend Const cTipoEntero As Byte = 3
        ''' <summary>
        ''' Constante de tipo de campo entero largo
        ''' </summary>
        Friend Const cTipoEnteroLargo As Byte = 4
        ''' <summary>
        ''' Constante de tipo de campo boolean
        ''' </summary>
        Friend Const cTipoBoolean As Byte = 5
        ''' <summary>
        ''' Constante de tipo de campo single
        ''' </summary>
        Friend Const cTipoSingle As Byte = 6
        ''' <summary>
        ''' Constante de tipo de campo decimal
        ''' </summary>
        Friend Const cTipoDecimal As Byte = 7
        ''' <summary>
        ''' Constante de tipo de campo Doble
        ''' </summary>
        Friend Const cTipoDoble As Byte = 8
        ''' <summary>
        ''' Constante de tipo de campo Fecha
        ''' </summary>
        Friend Const cTipoFecha As Byte = 9
        ''' <summary>
        ''' Constante de tipo de campo GUID
        ''' </summary>
        Friend Const cTipoGUID As Byte = 10

        'Definición de constantes.
        Friend Const cTipoAccess97 As Byte = 0
        Friend Const cTipoAccess2010 As Byte = 1
        Friend Const cTipoXML As Byte = 2
        Friend Const cTipoMDE As Byte = 3
        Friend Const cTipoMSSQL As Byte = 4

        'Variables privadas.
        Private _bEstado As Byte
        Private _nTipoDB As Byte = 0
        Private _dbConexión As New OleDb.OleDbConnection
        Private _sCadena_Conexión As String = ""
        Private _sBase_de_datos As String = ""
        Private _sRuta_mdw As String = ""
        Private _sUsuario_conexión As String = ""
        Private _sPassword_conexión As String = ""
        Private _oEstructura_Tablas As DataTable
        Private _oEstructura_Columnas As DataTable
        Private _sMensajeError As String = ""


        Public ReadOnly Property MensajeError As String
            Get
                Return _sMensajeError
            End Get
        End Property

        ''' <summary>
        ''' Especificación de los tipo de base de datos.
        ''' </summary>
        ''' <value>
        ''' 0: Para access 97
        ''' 1: Para Access 2003/2010
        ''' </value>
        ''' <returns>Tipo actual de la base de datos.</returns>
        ''' <remarks></remarks>
        Public Property TipoBaseDeDatos() As Byte
            Get
                Return _nTipoDB
            End Get
            Set(ByVal value As Byte)
                _nTipoDB = value
            End Set
        End Property

        Public ReadOnly Property EstructuraTablas As DataTable
            Get
                Return _oEstructura_Tablas
            End Get
        End Property

        Public ReadOnly Property EstructuraColumnas As DataTable
            Get
                Return _oEstructura_Columnas
            End Get
        End Property

        Private Function ConversiónTipoDatos(tipo_interno As Byte) As Byte
            Dim _nResultado As Byte = cTipoSindefinir

            Select Case tipo_interno
                Case 2
                    _nResultado = cTipoEntero 'Inclue los autonuméricos.
                Case 3
                    _nResultado = cTipoEnteroLargo
                Case 4
                    _nResultado = cTipoSingle
                Case 5
                    _nResultado = cTipoDoble
                Case 6 'Es tipo moneda, pero lo tratamos como single.
                    _nResultado = cTipoSingle
                Case 7
                    _nResultado = cTipoFecha
                Case 11
                    _nResultado = cTipoBoolean
                Case 17
                    _nResultado = cTipoBoolean
                Case 72
                    _nResultado = cTipoGUID
                Case 130
                    _nResultado = cTipoCadena
                Case 131
                    _nResultado = cTipoDecimal

            End Select

            Return _nResultado
        End Function

        Public Function TipoDatos(Tabla As String, Campo As String) As Byte
            Dim _nPuntero As Integer = 0
            Dim _nResultado As Byte = cTipoSindefinir
            Dim _bSalir As Boolean = False

            Try
                While Not _bSalir
                    If _nPuntero >= _oEstructura_Columnas.Rows.Count Then
                        _bSalir = True
                    Else
                        If Tabla = _oEstructura_Columnas(_nPuntero)("TABLE_NAME").ToString And Campo = _oEstructura_Columnas(_nPuntero)("COLUMN_NAME").ToString Then
                            _bSalir = True
                        Else
                            _nPuntero = _nPuntero + 1
                        End If
                    End If
                End While

                If _nPuntero < _oEstructura_Columnas.Rows.Count Then
                    _nResultado = ConversiónTipoDatos(CByte(_oEstructura_Columnas(_nPuntero)("DATA_TYPE")))
                End If
            Catch ex As Exception
                _oLog.Log("Error: Analizando tipos de datos: " & ex.Message)
            End Try
            
            Return _nResultado
        End Function

        Public Function TamañoDatos(Tabla As String, campo As String) As Byte
            Dim _nPuntero As Integer = 0
            Dim _nResultado As Byte = 0

            Try
                While _nPuntero < _oEstructura_Columnas.Rows.Count And Tabla <> _oEstructura_Columnas(_nPuntero)("TABLE_NAME").ToString And campo <> _oEstructura_Columnas(_nPuntero)("COLUMN_NAME").ToString
                    _nPuntero = _nPuntero + 16
                End While

                If _nPuntero < _oEstructura_Columnas.Rows.Count Then
                    _nResultado = CByte(_oEstructura_Columnas(_nPuntero)("CHARACTER_MAXIMUM_LENGTH"))
                End If
            Catch ex As Exception
                _oLog.Log("Error: Analizando tamño de los datos: " & ex.Message)
            End Try

            Return _nResultado
        End Function

        Public Property ArchivoMdw As String
            Get
                Return _sRuta_mdw
            End Get
            Set(value As String)
                _sRuta_mdw = value
            End Set
        End Property

        Public Property UsuarioConexión As String
            Get
                Return _sUsuario_conexión
            End Get
            Set(value As String)
                _sUsuario_conexión = value
            End Set
        End Property

        Public Property PasswordConexión As String
            Get
                Return _sPassword_conexión
            End Get
            Set(value As String)
                _sPassword_conexión = value
            End Set
        End Property

        ''' <summary>
        ''' Ruta de la base de datos incluyendo la extensión.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property BaseDeDatos() As String
            Get
                Return _sBase_de_datos
            End Get
            Set(value As String)
                Select Case Right(value, 3).ToString.ToUpper
                    Case "MDB"
                        TipoBaseDeDatos = cTipoAccess97
                        _sBase_de_datos = value
                    Case "CDB"
                        TipoBaseDeDatos = cTipoAccess2010
                        _sBase_de_datos = value
                    Case "MDE"
                        TipoBaseDeDatos = cTipoMDE
                        _sBase_de_datos = value
                    Case "XML"
                        TipoBaseDeDatos = cTipoXML
                        _sBase_de_datos = value
                    Case Else
                        TipoBaseDeDatos = cTipoMSSQL
                        _sBase_de_datos = ""
                        _sCadena_Conexión = value
                End Select

                If TipoBaseDeDatos <> cTipoMDE Then
                    If BDConectado Then
                        _dbConexión.Close()
                    End If
                    Conectar()
                End If
            End Set
        End Property

        ''' <summary>
        ''' Ruta de conexión para la base de datos
        ''' </summary>
        ''' <value></value>
        ''' <returns>Ruta completa incluido el arhivo</returns>
        ''' <remarks></remarks>
        Friend Property RutaConexión() As String
            Get
                Return _sCadena_Conexión
            End Get
            Set(ByVal value As String)
                _sCadena_Conexión = value
            End Set
        End Property

        ''' <summary>
        ''' Clase oleDB que mantiene la conexión con la base de datos
        ''' </summary>
        ''' <value>null</value>
        ''' <returns>OleDbConnection</returns>
        ''' <remarks></remarks>
        Public Property Conector() As OleDb.OleDbConnection
            Get
                Return _dbConexión
            End Get
            Set(ByVal value As OleDb.OleDbConnection)
                _dbConexión = value
            End Set
        End Property

        Public ReadOnly Property BDConectado As Boolean
            Get
                Dim _bResultado As Boolean = False

                If _dbConexión.State.ToString = "Open" Then
                    _bResultado = True
                End If

                Return _bResultado
            End Get
        End Property

        Public ReadOnly Property BDDesconectado As Boolean
            Get
                Dim _bResultado As Boolean = False

                If _dbConexión.State.ToString = "Closed" Then
                    _bResultado = True
                End If

                Return _bResultado
            End Get
        End Property

        Public ReadOnly Property BDRoto As Boolean
            Get
                Dim _bResultado As Boolean = False

                If _dbConexión.State.ToString = "Broken" Then
                    _bResultado = True
                End If

                Return _bResultado
            End Get
        End Property

        ''' <summary>
        ''' Establece una conexión nueva usando la propiedad Ruta_Conexión
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Conectar()
            Dim _nPuntero As Integer = 0

            Try
                If _sCadena_Conexión <> "" Then
                    _dbConexión = New OleDbConnection(_sCadena_Conexión)
                Else
                    Select Case _nTipoDB
                        Case cTipoAccess97
                            _sCadena_Conexión = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & _sBase_de_datos & ";"
                        Case cTipoAccess2010
                            _sCadena_Conexión = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & _sBase_de_datos & ";Persist Security Info=False"
                        Case cTipoMDE
                            _sCadena_Conexión = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & _sBase_de_datos & ";" & _
                                " Jet OLEDB:System database=" & _sRuta_mdw & "; User ID=" & _sUsuario_conexión & ";Password=" & _sPassword_conexión & ";"
                    End Select
                End If
                If _sCadena_Conexión <> "" Then
                    _dbConexión = New OleDbConnection(_sCadena_Conexión)
                    _dbConexión.Open()

                    If _nTipoDB <> cTipoMDE Then
                        _oEstructura_Tablas = _dbConexión.GetSchema("Tables")
                        _oEstructura_Columnas = _dbConexión.GetSchema("Columns")
                    End If
                End If

            Catch ex As Exception
                _oLog.Log("Error: Conexión con la base de datos: " & ex.Message)
            End Try
        End Sub

        Public Function Desconectar() As Boolean
            Dim _bResultado As Boolean = False

            Try
                _dbConexión.Close()
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Desconexión de la base de datos: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        ''' <summary>
        ''' Ejecución de línea SQL a una conexión.
        ''' </summary>
        ''' <param name="_sMandato">Instrucción SQL standard</param>
        ''' <returns>verdade o false si se ha ejecutado correctamente.</returns>
        ''' <remarks></remarks>
        Public Function SQLExec(ByVal _sMandato As String) As Boolean
            Dim _comando As New System.Data.OleDb.OleDbCommand(_sMandato.Trim, _dbConexión)
            Dim _bResultado As Boolean

            Try
                _comando.ExecuteNonQuery()
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Ejecutando SQL: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        Public Function Crear_MDB_2003(ruta As String) As Boolean
            Dim _bResultado As Boolean
            Try
                Dim s As System.IO.Stream = System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream("eva.db_2003.mdb")
                Dim ResourceFile As New System.IO.FileStream(ruta, IO.FileMode.Create)

                Dim b(CInt(s.Length)) As Byte

                s.Read(b, 0, CInt(s.Length))
                ResourceFile.Write(b, 0, b.Length - 1)
                ResourceFile.Flush()
                ResourceFile.Close()
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Crear mdb 2003: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        Public Function Crear_MDB_2010(ruta As String) As Boolean
            Dim _bResultado As Boolean
            Try
                Dim s As System.IO.Stream = System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream("eva.db_2010.accdb")
                Dim ResourceFile As New System.IO.FileStream(ruta, IO.FileMode.Create)

                Dim b(CInt(s.Length)) As Byte

                s.Read(b, 0, CInt(s.Length))
                ResourceFile.Write(b, 0, b.Length - 1)
                ResourceFile.Flush()
                ResourceFile.Close()
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Crear mdb 2010: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        ''' <summary>
        ''' Constructor de la clase.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()

        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class
End Namespace