﻿Option Explicit On
Option Strict On
Imports System.Management
Imports System.Reflection.Assembly
Imports System.IO
Imports System.Windows.Forms

Namespace FrameWork
    Public Class cls_Config_xml
        Private _oConfigXML As New DataSet
        Private _oLog As New cls_Log

        Private _sTemporal As String = ""
        Private _sAplicación As String = ""
        Private _sRuta As String = ""

        Private _sUrl As String = ""
        Private _sEmpresa_ID As String = ""
        Private _sdisp_ID As String = ""

        Private _sPrincipal_BD As String = ""
        Private _sNormalizada_BD As String = ""
        Private _sTrabajo_dir As String = ""

        Private _nVersión_Internet As Long = 0
        Private _sUrlDownload As String = ""
        Private _nFecha As UInteger = 0
        Private _sEstado As String = ""
        Private _sAviso As String = ""

        Dim _bModificado As Boolean = False
        Dim _bCreado As Boolean = False

        Private Function TraductorDeCadena(cadena As String) As String
            Dim _oSistema As New cls_USF()
            Dim _sResultado As String = ""

            _sResultado = cadena
            _sResultado = _sResultado.Replace("[ruta_temporal]", _oSistema.ruta_carpeta_temporal)
            _sResultado = _sResultado.Replace("[ruta_windows]", _oSistema.ruta_carpeta_windows)
            _sResultado = _sResultado.Replace("[ruta_actual]", _oSistema.ruta_actual)
            _sResultado = _sResultado.Replace("[ruta_archivos_de_programas]", _oSistema.ruta_activos_de_programas)
            _sResultado = _sResultado.Replace("[hd_serial]", _oSistema.Disco_duro_serial)
            _sResultado = _sResultado.Replace("[procesador_id]", _oSistema.Procesador_ID)
            _sResultado = _sResultado.Replace("[placa_id]", _oSistema.Placa_base_ID)
            _sResultado = _sResultado.Replace("[nombre_equipo]", _oSistema.Nombre_equipo)
            _sResultado = _sResultado.Replace("[nombre_usuario]", _oSistema.Nombre_Usuario)
            _sResultado = _sResultado.Replace("[mi_ip]", _oSistema.mi_ip)
            _sResultado = _sResultado.Replace("[localapp]", _oSistema.Leer_Variable_Entorno("LOCALAPPDATA").ToString())
            If _oSistema.es_64bits Then
                _sResultado = _sResultado.Replace("[ruta_archivos_de_programas_x86]", _oSistema.ruta_archivos_de_programas_x86)
            End If

            Return _sResultado
        End Function

        Private Function DLLPath() As String
            Dim _sResultado As String = IO.Path.GetDirectoryName(GetExecutingAssembly.Location) & "\"

            Return _sResultado
        End Function

        Private Function DLLName(Optional ByVal fullPath As Boolean = False) As String
            Dim _sResultado As String = GetExecutingAssembly.Location
            Dim fi As New IO.FileInfo(_sResultado)

            _sResultado = fi.FullName

            Return _sResultado
        End Function

        Private Sub Verifica_Directorio(ruta As String)
            Dim _oArchivos As New eva.FrameWork.cls_UAF

            Try
                _oArchivos.Ruta = ruta
                If Not _oArchivos.Existe Then
                    _oArchivos.Crea_Directorio()
                End If
            Catch ex As Exception
                _oLog.Log("Error: Verificando / Creando directorio. " & ex.Message)
            End Try
            
        End Sub

        Public ReadOnly Property Online_Version As Long
            Get
                Return _nVersión_Internet
            End Get
        End Property

        Public ReadOnly Property Online_Fecha As UInteger
            Get
                Return _nFecha
            End Get
        End Property

        Public ReadOnly Property Online_Estado As String
            Get
                Return _sEstado
            End Get
        End Property

        Public ReadOnly Property Online_Aviso As String
            Get
                Return _sAviso
            End Get
        End Property

        Public ReadOnly Property Descarga_URL As String
            Get
                Return _sUrlDownload
            End Get
        End Property

        Public Property cmp_CONFIG_ver As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("config").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("config")(0)("ver").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("config").Rows.Count = 0 Then _oConfigXML.Tables("config").Rows.Add()
                _oConfigXML.Tables("config")(0)("ver") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo config::ver")
            End Set
        End Property

        Public Property cmp_APP_id As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("app").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("app")(0)("id").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("app").Rows.Count = 0 Then _oConfigXML.Tables("app").Rows.Add()
                _oConfigXML.Tables("app")(0)("id") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo app::id")
            End Set
        End Property

        Public Property cmp_APP_ver As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("app").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("app")(0)("ver").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("app").Rows.Count = 0 Then _oConfigXML.Tables("app").Rows.Add()
                _oConfigXML.Tables("app")(0)("ver") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo app::ver")
            End Set
        End Property

        Public Property cmp_APP_hash As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("app").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("app")(0)("hash").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("app").Rows.Count = 0 Then _oConfigXML.Tables("app").Rows.Add()
                _oConfigXML.Tables("app")(0)("hash") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo app::ver")
            End Set
        End Property

        Public Property cmp_APP_url As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("app").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("app")(0)("url").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("app").Rows.Count = 0 Then _oConfigXML.Tables("app").Rows.Add()
                _oConfigXML.Tables("app")(0)("url") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo app::url")
            End Set
        End Property

        Public Property cmp_APP_empresa_ID As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("app").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("app")(0)("empresa_ID").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("app").Rows.Count = 0 Then _oConfigXML.Tables("app").Rows.Add()
                _oConfigXML.Tables("app")(0)("empresa_ID") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo app::empresa_id")
            End Set
        End Property

        Public Property cmp_APP_disp_ID As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("app").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("app")(0)("disp_ID").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("app").Rows.Count = 0 Then _oConfigXML.Tables("app").Rows.Add()
                _oConfigXML.Tables("app")(0)("disp_ID") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo app::disp_id")
            End Set
        End Property

        Public Property cmp_APP_email_notif As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("app").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("app")(0)("email_notif").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("app").Rows.Count = 0 Then _oConfigXML.Tables("app").Rows.Add()
                _oConfigXML.Tables("app")(0)("email_notif") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo app::email_notif")
            End Set
        End Property

        Public Property cmp_APP_splash_screen As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("app").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("app")(0)("splash_screen").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("app").Rows.Count = 0 Then _oConfigXML.Tables("app").Rows.Add()
                _oConfigXML.Tables("app")(0)("splash_screen") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo app::splash_screen")
            End Set
        End Property

        Public Property cmp_RUTA_servidor As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("ruta_datos").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("ruta_datos")(0)("servidor").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("ruta_datos").Rows.Count = 0 Then _oConfigXML.Tables("ruta_datos").Rows.Add()
                _oConfigXML.Tables("ruta_datos")(0)("servidor") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo ruta::principa_bd")
            End Set
        End Property

        Public Property cmp_RUTA_principal_BD As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("ruta_datos").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("ruta_datos")(0)("principal_bd").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("ruta_datos").Rows.Count = 0 Then _oConfigXML.Tables("ruta_datos").Rows.Add()
                _oConfigXML.Tables("ruta_datos")(0)("principal_bd") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo ruta::principa_bd")
            End Set
        End Property

        Public Property cmp_RUTA_normalizada_BD As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("ruta_datos").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("ruta_datos")(0)("normalizada_bd").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("ruta_datos").Rows.Count = 0 Then _oConfigXML.Tables("ruta_datos").Rows.Add()
                _oConfigXML.Tables("ruta_datos")(0)("normalizada_bd") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo ruta::normalizada_bd")
            End Set
        End Property

        Public Property cmp_RUTA_trabajo_Dir As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("ruta_datos").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("ruta_datos")(0)("trabajo_dir").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("ruta_datos").Rows.Count = 0 Then _oConfigXML.Tables("ruta_datos").Rows.Add()
                _oConfigXML.Tables("ruta_datos")(0)("trabajo_dir") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo ruta::rabajo_dir")
            End Set
        End Property

        Public Property cmp_RUTA_usuario As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("ruta_datos").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("ruta_datos")(0)("usuario").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("ruta_datos").Rows.Count = 0 Then _oConfigXML.Tables("ruta_datos").Rows.Add()
                _oConfigXML.Tables("ruta_datos")(0)("usuario") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo ruta::usuario")
            End Set
        End Property

        Public Property cmp_RUTA_password As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("ruta_datos").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("ruta_datos")(0)("password").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("ruta_datos").Rows.Count = 0 Then _oConfigXML.Tables("ruta_datos").Rows.Add()
                _oConfigXML.Tables("ruta_datos")(0)("password") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo ruta::password")
            End Set
        End Property

        Public Property cmp_SERVICIOS_id As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("servicios").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("servicios")(0)("id").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("servicios").Rows.Count = 0 Then _oConfigXML.Tables("servicios").Rows.Add()
                _oConfigXML.Tables("servicios")(0)("id") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo servicios-id")
            End Set
        End Property

        Public Property cmp_SERVICIOS_url As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("servicios").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("servicios")(0)("url").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("servicios").Rows.Count = 0 Then _oConfigXML.Tables("servicios").Rows.Add()
                _oConfigXML.Tables("servicios")(0)("url") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo servicios-url")
            End Set
        End Property

        Public Property cmp_SERVICIOS_usuario As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("servicios").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("servicios")(0)("usuario").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get

            Set(value As String)
                If _oConfigXML.Tables("servicios").Rows.Count = 0 Then _oConfigXML.Tables("servicios").Rows.Add()
                _oConfigXML.Tables("servicios")(0)("usuario") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo servicios-usuario")
            End Set
        End Property

        Public Property cmp_SERVICIOS_password As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("servicios").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("servicios")(0)("password").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get

            Set(value As String)
                If _oConfigXML.Tables("servicios").Rows.Count = 0 Then _oConfigXML.Tables("password").Rows.Add()
                _oConfigXML.Tables("servicios")(0)("password") = TraductorDeCadena(value)
                _oLog.Log("aviso: Modificado nodo servicios-password")
            End Set
        End Property

        Public Property cmp_EMAIL_email As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("email_config").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("email_config")(0)("email").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("email_config").Rows.Count = 0 Then _oConfigXML.Tables("email_config").Rows.Add()
                _oConfigXML.Tables("email_config")(0)("email") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo email::email")
            End Set
        End Property

        Public Property cmp_EMAIL_usuario As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("email_config").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("email_config")(0)("usuario").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("email_config").Rows.Count = 0 Then _oConfigXML.Tables("email_config").Rows.Add()
                _oConfigXML.Tables("email_config")(0)("usuario") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo email::usuario")
            End Set
        End Property

        Public Property cmp_EMAIL_password As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("email_config").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("email_config")(0)("password").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("email_config").Rows.Count = 0 Then _oConfigXML.Tables("email_config").Rows.Add()
                _oConfigXML.Tables("email_config")(0)("password") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo email::password")
            End Set
        End Property

        Public Property cmp_EMAIL_smtp As String
            Get
                Dim _sResultado As String = ""

                Try
                    If _oConfigXML.Tables("email_config").Rows.Count > 0 Then _sResultado = _oConfigXML.Tables("email_config")(0)("smtp").ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
            Set(value As String)
                If _oConfigXML.Tables("email_config").Rows.Count = 0 Then _oConfigXML.Tables("email_config").Rows.Add()
                _oConfigXML.Tables("email_config")(0)("smtp") = TraductorDeCadena(value)
                _oLog.Log("Aviso: Modificado nodo email::smtp")
            End Set
        End Property

        Public Property cmp_EMAIL_puerto As Integer
            Get
                Dim _nResultado As Integer = 0

                Try
                    If _oConfigXML.Tables("email_config").Rows.Count > 0 Then _nResultado = CInt(_oConfigXML.Tables("email_config")(0)("puerto"))
                Catch ex As Exception
                    _nResultado = 0
                End Try

                Return _nResultado
            End Get
            Set(value As Integer)
                If _oConfigXML.Tables("email_config").Rows.Count = 0 Then _oConfigXML.Tables("email_config").Rows.Add()
                _oConfigXML.Tables("email_config")(0)("puerto") = value
                _oLog.Log("Aviso: Modificado nodo email::puerto")
            End Set
        End Property

        Public Property cmp_EMAIL_ssl As Boolean
            Get
                Dim _nResultado As Boolean = False

                Try
                    If _oConfigXML.Tables("email_config").Rows.Count > 0 Then _nResultado = CBool(_oConfigXML.Tables("email_config")(0)("ssl"))
                Catch ex As Exception
                    _nResultado = False
                End Try

                Return _nResultado
            End Get
            Set(value As Boolean)
                If _oConfigXML.Tables("email_config").Rows.Count = 0 Then _oConfigXML.Tables("email_config").Rows.Add()
                _oConfigXML.Tables("email_config")(0)("ssl") = value
                _oLog.Log("Aviso: Modificado nodo email::ssl")
            End Set
        End Property

        Public ReadOnly Property Ruta_Temporales As String
            Get
                Return _sTemporal
            End Get
        End Property

        Public ReadOnly Property APP As String
            Get
                Return _sAplicación
            End Get
        End Property

        Public ReadOnly Property Ruta_APP As String
            Get
                Return _sRuta
            End Get
        End Property

        Public ReadOnly Property Estructura_ConfigXML As DataSet
            Get
                Return _oConfigXML
            End Get
        End Property

        Public Property Modificado As Boolean
            Get
                Return _bModificado
            End Get
            Set(value As Boolean)
                _bModificado = value
            End Set
        End Property

        Public Sub Verificar_Estructura_ConfigXML(app As String)
            Dim _oSistemas As New eva.FrameWork.cls_USF
            Dim _oArchivos As New eva.FrameWork.cls_UAF

            _sAplicación = app
            _sRuta = _oSistemas.Leer_Variable_Entorno("LOCALAPPDATA").ToString() & "\" & _sAplicación
            Verifica_Directorio(_sRuta)
            Verifica_Directorio(_sRuta & "\tmp")
            Verifica_Directorio(_sRuta & "\multimedia")
            Verifica_Directorio(_sRuta & "\multimedia\imágenes")
            Verifica_Directorio(_sRuta & "\multimedia\video")
            Verifica_Directorio(_sRuta & "\multimedia\audio")

            _oArchivos.Ruta = _sRuta & "\config.xml"
            If Not _oArchivos.Existe Then
                _oConfigXML.DataSetName = "configxml"

                _oConfigXML.Tables.Add("config")
                _oConfigXML.Tables("config").Columns.Add("ver", Type.GetType("System.String"))
                _oConfigXML.Tables("config").Rows.Add("14.04.01")

                _oConfigXML.Tables.Add("app")
                _oConfigXML.Tables("app").Columns.Add("id", Type.GetType("System.String"))
                _oConfigXML.Tables("app").Columns.Add("ver", Type.GetType("System.String"))
                _oConfigXML.Tables("app").Columns.Add("hash", Type.GetType("System.String"))
                _oConfigXML.Tables("app").Columns.Add("url", Type.GetType("System.String"))
                _oConfigXML.Tables("app").Columns.Add("empresa_ID", Type.GetType("System.String"))
                _oConfigXML.Tables("app").Columns.Add("disp_ID", Type.GetType("System.String"))
                _oConfigXML.Tables("app").Columns.Add("email_notif", Type.GetType("System.String"))
                _oConfigXML.Tables("app").Columns.Add("splash_screen", Type.GetType("System.String"))
                _oConfigXML.Tables("app").Rows.Add(_sAplicación, "", "", "", "", "", "", "")

                _oConfigXML.Tables.Add("ruta_datos")
                _oConfigXML.Tables("ruta_datos").Columns.Add("servidor", Type.GetType("System.String"))
                _oConfigXML.Tables("ruta_datos").Columns.Add("principal_bd", Type.GetType("System.String"))
                _oConfigXML.Tables("ruta_datos").Columns.Add("normalizada_bd", Type.GetType("System.String"))
                _oConfigXML.Tables("ruta_datos").Columns.Add("trabajo_dir", Type.GetType("System.String"))
                _oConfigXML.Tables("ruta_datos").Columns.Add("usuario", Type.GetType("System.String"))
                _oConfigXML.Tables("ruta_datos").Columns.Add("password", Type.GetType("System.String"))
                _oConfigXML.Tables("ruta_datos").Rows.Add("", "", "", "", "", "")

                _oConfigXML.Tables.Add("servicios")
                _oConfigXML.Tables("servicios").Columns.Add("id", Type.GetType("System.String"))
                _oConfigXML.Tables("servicios").Columns.Add("url", Type.GetType("System.String"))
                _oConfigXML.Tables("servicios").Columns.Add("usuario", Type.GetType("System.String"))
                _oConfigXML.Tables("servicios").Columns.Add("password", Type.GetType("System.String"))

                _oConfigXML.Tables.Add("email_config")
                _oConfigXML.Tables("email_config").Columns.Add("email", Type.GetType("System.String"))
                _oConfigXML.Tables("email_config").Columns.Add("usuario", Type.GetType("System.String"))
                _oConfigXML.Tables("email_config").Columns.Add("password", Type.GetType("System.String"))
                _oConfigXML.Tables("email_config").Columns.Add("smtp", Type.GetType("System.String"))
                _oConfigXML.Tables("email_config").Columns.Add("puerto", Type.GetType("System.Int32"))
                _oConfigXML.Tables("email_config").Columns.Add("ssl", Type.GetType("System.Boolean"))

                _oConfigXML.Tables.Add("scanner")
                _oConfigXML.Tables("scanner").Columns.Add("disp", Type.GetType("System.String"))
                _oConfigXML.Tables("scanner").Columns.Add("x1", Type.GetType("System.Single"))
                _oConfigXML.Tables("scanner").Columns.Add("y1", Type.GetType("System.Single"))
                _oConfigXML.Tables("scanner").Columns.Add("x2", Type.GetType("System.Single"))
                _oConfigXML.Tables("scanner").Columns.Add("y2", Type.GetType("System.Single"))

                _oConfigXML.Tables.Add("logs")
                _oConfigXML.Tables("logs").Columns.Add("timestamp", Type.GetType("System.String"))
                _oConfigXML.Tables("logs").Columns.Add("log", Type.GetType("System.String"))
                _oConfigXML.Tables("logs").Rows.Add(Now.ToString, "configXML: Creación de estructura de datos.")
                _bCreado = True
            End If
        End Sub

        Private Sub Migrar_desde_131018()
            Dim _oTemp As New cls_Config_xml
            Dim _oArchivo As New eva.FrameWork.cls_UAF

            _oArchivo.Ruta = _sRuta & "\config.xml"
            If _oArchivo.Existe Then _oArchivo.Borrar()
            _oArchivo.Ruta = _sRuta & "\config.xsd"
            If _oArchivo.Existe Then _oArchivo.Borrar()

            _oTemp.Verificar_Estructura_ConfigXML(_sAplicación)

            _oTemp.cmp_APP_id = Me.cmp_APP_id
            _oTemp.cmp_APP_ver = Me.cmp_APP_ver
            _oTemp.cmp_APP_hash = ""
            _oTemp.cmp_APP_url = Me.cmp_APP_url
            _oTemp.cmp_APP_empresa_ID = Me.cmp_APP_empresa_ID
            _oTemp.cmp_APP_disp_ID = Me.cmp_APP_disp_ID
            _oTemp.cmp_APP_email_notif = Me.cmp_APP_email_notif
            _oTemp.cmp_APP_splash_screen = ""

            _oTemp.cmp_RUTA_principal_BD = Me.cmp_RUTA_principal_BD
            _oTemp.cmp_RUTA_normalizada_BD = Me.cmp_RUTA_normalizada_BD
            _oTemp.cmp_RUTA_trabajo_Dir = Me.cmp_RUTA_trabajo_Dir
            _oTemp.cmp_RUTA_usuario = ""
            _oTemp.cmp_RUTA_password = ""

            _oTemp.cmp_EMAIL_email = Me.cmp_EMAIL_email
            _oTemp.cmp_EMAIL_usuario = Me.cmp_EMAIL_usuario
            _oTemp.cmp_EMAIL_password = Me.cmp_EMAIL_password
            _oTemp.cmp_EMAIL_smtp = Me.cmp_EMAIL_smtp
            _oTemp.cmp_EMAIL_puerto = Me.cmp_EMAIL_puerto
            _oTemp.cmp_EMAIL_ssl = True

            _oTemp.Grabar_ConfigXML()
        End Sub

        Public Function Leer_ConfigXML(app As String) As Boolean
            Dim _oSistema As New eva.FrameWork.cls_USF
            Dim _bResultado As Boolean = False

            _sAplicación = app
            _sRuta = _oSistema.Leer_Variable_Entorno("LOCALAPPDATA").ToString & "\" & app
            _sTemporal = _oSistema.Leer_Variable_Entorno("LOCALAPPDATA").ToString & "\" & app & "\tmp"
            Try
                _oConfigXML.ReadXmlSchema(_sRuta & "\config.xsd")
                _oConfigXML.ReadXml(_sRuta & "\config.xml")

                If Me.cmp_CONFIG_ver = "13.10.28" Then
                    Migrar_desde_131018()
                    _oConfigXML = Nothing
                    _oConfigXML = New DataSet
                    _oConfigXML.ReadXmlSchema(_sRuta & "\config.xsd")
                    _oConfigXML.ReadXml(_sRuta & "\config.xml")

                    Me.NuevoLog("AVISO: Actualización desde versión 13.10.28")
                End If

                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: En lectura de archivo de configuración. " & ex.Message)
            End Try

            Return _bResultado
        End Function

        Private Function _Limpieza_config() As Boolean
            Dim _nPuntero As Integer = _oConfigXML.Tables("config").Rows.Count
            Dim _bResultado As Boolean = False

            Try
                If _nPuntero > 1 Then _bResultado = True
                While _nPuntero > 1
                    _oConfigXML.Tables("config").Rows(_nPuntero - 1).Delete()
                    _nPuntero = _nPuntero - 1
                End While

            Catch ex As Exception
                _oLog.Log("Error: Vaciando el archivo de configuración." & ex.Message)
            End Try

            Return _bResultado
        End Function

        Private Function _Limpieza_app() As Boolean
            Dim _nPuntero As Integer = _oConfigXML.Tables("app").Rows.Count
            Dim _bResultado As Boolean = False

            If _nPuntero > 1 Then _bResultado = True
            While _nPuntero > 1
                _oConfigXML.Tables("app").Rows(_nPuntero - 1).Delete()
                _nPuntero = _nPuntero - 1
            End While

            Return _bResultado
        End Function

        Private Function _Limpieza_ruta_datos() As Boolean
            Dim _nPuntero As Integer = _oConfigXML.Tables("ruta_datos").Rows.Count
            Dim _bResultado As Boolean = False

            If _nPuntero > 1 Then _bResultado = True
            While _nPuntero > 1
                _oConfigXML.Tables("ruta_datos").Rows(_nPuntero - 1).Delete()
                _nPuntero = _nPuntero - 1
            End While
            _bResultado = True

            Return _bResultado
        End Function

        Private Function _Limpieza_email_config() As Boolean
            Dim _nPuntero As Integer = _oConfigXML.Tables("email_config").Rows.Count
            Dim _bResultado As Boolean = False

            If _nPuntero > 1 Then _bResultado = True
            While _nPuntero > 1
                _oConfigXML.Tables("email_config").Rows(_nPuntero - 1).Delete()
                _nPuntero = _nPuntero - 1
            End While

            _bResultado = True

            Return _bResultado
        End Function

        Private Function _Limpieza_logs() As Boolean
            Dim _nPuntero As Integer = _oConfigXML.Tables("logs").Rows.Count
            Dim _bResultado As Boolean = False

            If _nPuntero > 10 Then
                While _nPuntero > 15
                    _oConfigXML.Tables("logs").Rows(_nPuntero - 1).Delete()
                    _nPuntero = _nPuntero - 1
                End While
            End If

            _bResultado = True

            Return _bResultado
        End Function

        Public Sub Grabar_ConfigXML()
            Dim _oArchivos As New cls_UAF

            Try
                If _bModificado Or _Limpieza_config() Or _Limpieza_app() Or _Limpieza_ruta_datos() Or _Limpieza_email_config() Or _Limpieza_logs() Then
                    _oConfigXML.WriteXmlSchema(_sRuta & "\config.xsd")

                    _oArchivos.Ruta = _sRuta & "\config.xml"

                    If _oArchivos.Existe Then _oArchivos.Borrar()
                    _oConfigXML.WriteXml(_sRuta & "\config.xml")
                End If

                _bModificado = False
            Catch ex As Exception
                _oLog.Log("Error: Grabando archivo de configuración. " & ex.Message)
            End Try

            
        End Sub

        Public Sub NuevoLog(Mensaje As String)
            _oConfigXML.Tables("logs").Rows.Add(Now.ToString, Mensaje)
            _bModificado = True
        End Sub

        Public Function Recopilar_datos_online() As Boolean
            Dim _oLicencia As New DataSet
            Dim _bResultado As Boolean = False
            Dim _nPuntero As Integer = 0

            _nVersión_Internet = 0
            _sUrlDownload = ""
            _nFecha = 0
            _sEstado = ""
            _sAviso = ""
            Try
                _oLicencia.ReadXml(cmp_APP_url & "/api.php?app=" & _sAplicación & "&empresa=" & Me.cmp_APP_empresa_ID & "&disp=" & Me.cmp_APP_disp_ID)
                While _nPuntero < _oLicencia.Tables("licencia").Rows.Count And _bResultado = False
                    If cmp_APP_empresa_ID = _oLicencia.Tables("licencia").Rows(_nPuntero)("empresa_id").ToString Then
                        _nVersión_Internet = CUInt(_oLicencia.Tables("licencia").Rows(_nPuntero)("version").ToString.Replace(".", ""))
                        _sUrlDownload = _oLicencia.Tables("licencia").Rows(_nPuntero)("url").ToString
                        _nFecha = CUInt(_oLicencia.Tables("licencia").Rows(_nPuntero)("fecha").ToString.Replace(".", ""))
                        _sEstado = _oLicencia.Tables("licencia").Rows(_nPuntero)("estado").ToString
                        _sAviso = _oLicencia.Tables("licencia").Rows(_nPuntero)("aviso").ToString
                        _bResultado = True
                    End If
                    _nPuntero = _nPuntero + 1
                End While
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Recuperando datos online. " & ex.Message)
            End Try

            Return _bResultado
        End Function

        Public Function Fecha_del_Servidor() As UInteger
            Dim _oFechaInternet As New DataSet
            Dim _nResultado As UInteger = 0

            Try
                _oFechaInternet.ReadXml(cmp_APP_url & "/hoy.php")
                _nResultado = CUInt(_oFechaInternet.Tables("hoy").Rows(0)("dia").ToString.Replace(".", ""))
            Catch ex As Exception
                _nResultado = 0
                _oLog.Log("Error: Leyendo fecha del servidor remoto. " & ex.Message)
            End Try

            Return _nResultado
        End Function

        Public Function NecesitaActualización() As Boolean
            Dim _bResultado As Boolean = False

            Try
                If Recopilar_datos_online() And Fecha_del_Servidor() > 0 Then
                    If CUInt(cmp_APP_ver.Replace(".", "")) < CUInt(_nVersión_Internet) Then
                        If _nFecha <= Fecha_del_Servidor() And _sEstado = "1" Then
                            _bResultado = True
                        Else
                            _bResultado = False
                        End If
                    Else
                        _bResultado = False
                    End If
                Else
                    _bResultado = False
                End If
            Catch ex As Exception
                _oLog.Log("Error: Comprobando necesidad de actulaización. " & ex.Message)
            End Try

            Return _bResultado
        End Function

        Public Function Descargar_Actualización(Optional Silencio As Boolean = False) As Boolean
            Dim _oEjecutar As New eva.FrameWork.cls_UAF
            Dim _oDescarga As New eva.FrameWork.cls_WF
            Dim _bResultado As Boolean = False

            Try
                If NecesitaActualización() Then
                    Dim _frm_Act As New frm_Actualización
                    _frm_Act.Show()
                    Application.DoEvents()
                    NuevoLog("AVISO: Descarga del archivo: " & _sUrlDownload)
                    _oEjecutar.Ruta = _sTemporal & "\" & _sAplicación & ".exe"
                    If _oEjecutar.Existe Then _oEjecutar.Borrar()
                    _oEjecutar.Ruta = _sTemporal & "\" & _sAplicación & ".zip"
                    If _oEjecutar.Existe Then _oEjecutar.Borrar()

                    _oDescarga.Descarga_Archivos(_sUrlDownload, _sTemporal & "\" & _sAplicación & ".zip")
                    _oEjecutar.Ruta = _sTemporal & "\" & _sAplicación & ".zip"
                    _frm_Act = Nothing
                    If _oEjecutar.Existe Then
                        _oEjecutar.UnZIP(_sTemporal)
                        _oEjecutar.Ruta = _sTemporal & "\" & _sAplicación & ".exe"
                        If Silencio Then _oEjecutar.Parámetros = "/silent"
                        _oEjecutar.Esperar = False
                        If _oEjecutar.Existe Then _oEjecutar.Ejecutar()
                        _bResultado = True
                    Else
                        NuevoLog("ERROR: Imposible ejecutar: " & _oEjecutar.Ruta)
                        _bResultado = False
                    End If
                Else
                    _bResultado = False
                End If
            Catch ex As Exception
                _oLog.Log("Error: En el proceso de verificación, descarga y ejecución de actualización. " & ex.Message)
                _bResultado = False
            End Try

            Return _bResultado
        End Function

        Public Sub Borrar_Estructura(APP As String)
            Dim _oArchivo As New eva.FrameWork.cls_UAF

            Try
                _oArchivo.Ruta = Ruta_ConfigXML(APP) & "\config.xml"
                If _oArchivo.Existe Then _oArchivo.Borrar()
                _oArchivo.Ruta = Ruta_ConfigXML(APP) & "\config.xsd"
                If _oArchivo.Existe Then _oArchivo.Borrar()
            Catch ex As Exception
                _oLog.Log("Error: Borrando estructura de configuración. " & ex.Message)
            End Try
        End Sub

        Public Function Ruta_ConfigXML(Optional app As String = "") As String
            Dim _oSistema As New eva.FrameWork.cls_USF
            Dim _sResultado As String = ""

            If app = "" Then
                _sResultado = _oSistema.Leer_Variable_Entorno("LOCALAPPDATA") & "\" & _sAplicación
            Else
                _sResultado = _oSistema.Leer_Variable_Entorno("LOCALAPPDATA") & "\" & app
            End If

            Return _sResultado
        End Function

        Public Function Existe_Configuración(app As String) As Boolean
            Dim _oSistema As New eva.FrameWork.cls_USF
            Dim _oDirectorio As New eva.FrameWork.cls_UAF
            Dim _bResultado As Boolean = False

            _oDirectorio.Ruta = _oSistema.Leer_Variable_Entorno("LOCALAPPDATA") & "\" & app & "\config.xml"
            If _oDirectorio.Existe Then _bResultado = True

            Return _bResultado
        End Function

        Public Sub New()

        End Sub
    End Class
End Namespace
