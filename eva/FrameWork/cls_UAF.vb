﻿Option Explicit On
Option Strict On

Imports System.IO
Imports Microsoft.Win32
Imports System.Diagnostics
Imports Ionic.Zip

Namespace FrameWork
    ''' <summary>
    ''' Gestión y tratamientos de archivos.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class cls_UAF
        Dim _sRuta As String = ""
        Dim _sContenido As String = ""
        Dim _bEsperar As Boolean = False
        Dim _bBorrar As Boolean = False
        Dim _sInstrucción As String = ""
        Dim _sParámetros As String = ""
        Private _oLog As New eva.FrameWork.cls_Log

        ''' <summary>
        ''' Comprueba si ruta es un directorio.
        ''' </summary>
        ''' <returns>Verdadero si lo es, falso en caso contrario.</returns>
        ''' <remarks></remarks>
        Private Function _Es_Directorio() As Boolean
            Dim _bResultado As Boolean = False

            Try
                If System.IO.Directory.Exists(Me._sRuta) Then _bResultado = True
            Catch ex As Exception
                _bResultado = True
                _oLog.Log("Error: Verificando directorio: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        ''' <summary>
        ''' Comrpueba si ruta es un archivo
        ''' </summary>
        ''' <returns>Verdadero si lo es, falso en caso contrario.</returns>
        Private Function _Es_Archivo() As Boolean
            Dim _bResultado As Boolean = False

            Try
                If System.IO.File.Exists(Me._sRuta) Then _bResultado = True
            Catch ex As Exception
                _bResultado = True
                _oLog.Log("Error: Verificando archivo" & ex.Message)
            End Try

            Return _bResultado
        End Function

        ''' <summary>
        ''' Indica la ruta del archivo
        ''' </summary>
        ''' <value>Ruta completa del archivo incluido el archivo y su extensión</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Ruta As String
            Get
                Return _sRuta
            End Get
            Set(ByVal value As String)
                _sRuta = value
            End Set
        End Property

        ''' <summary>
        ''' Indica si debe esperar a terminar la ejecución de la aplicación o no
        ''' </summary>
        ''' <value>falso</value>
        ''' <returns>true/false</returns>
        ''' <remarks></remarks>
        Public Property Esperar() As Boolean
            Get
                Return _bEsperar
            End Get
            Set(ByVal value As Boolean)
                _bEsperar = value
            End Set
        End Property

        ''' <summary>
        ''' Parámetros para la ejecución del archivo
        ''' </summary>
        ''' <value>Cadena con los parámetros</value>
        Public Property Parámetros As String
            Get
                Return _sParámetros
            End Get
            Set(ByVal value As String)
                _sParámetros = value
            End Set
        End Property

        ''' <summary>
        ''' Indica si existe o nó el archivo.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Existe As Boolean
            Get
                Dim _bResultado As Boolean = False
                If Directory.Exists(_sRuta) Then _bResultado = True
                If File.Exists(_sRuta) Then _bResultado = True

                Return _bResultado
            End Get
        End Property

        Public ReadOnly Property Tipo As String
            Get
                Dim _sResultado As String = ""

                Try
                    _sResultado = My.Computer.FileSystem.GetFileInfo(_sRuta).Extension
                Catch ex As Exception
                    _sResultado = ""
                End Try

                Return _sResultado
            End Get
        End Property

        Public ReadOnly Property Tamaño As Long
            Get
                Dim _nResultado As Long = 0

                Try
                    _nResultado = My.Computer.FileSystem.GetFileInfo(_sRuta).Length
                Catch ex As Exception
                    _nResultado = 0
                    _oLog.Log("Error: Verificando tamaño del archivo: " & ex.Message)
                End Try

                Return _nResultado
            End Get
        End Property

        Public ReadOnly Property Nombre As String
            Get
                Dim _sResultado As String = ""

                Try
                    _sResultado = My.Computer.FileSystem.GetFileInfo(_sRuta).Name
                Catch ex As Exception
                    _sResultado = ""
                    _oLog.Log("Error: Verificando nombre del archivo: " & ex.Message)
                End Try

                Return _sResultado
            End Get
        End Property

        ''' <summary>
        ''' Contenido del archivo.
        ''' </summary>
        ''' <value>Contenido del archivos tras leer</value>
        ''' <returns>Contenido del archivo.</returns>
        ''' <remarks></remarks>
        Public Property Contenido As String
            Get
                Return _sContenido
            End Get
            Set(ByVal value As String)
                _sContenido = value
            End Set
        End Property

        ''' <summary>
        ''' Ejecuta la aplicación según los parámetros marcados.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Ejecutar()
            Dim objProcess As System.Diagnostics.Process

            Try
                objProcess = New System.Diagnostics.Process()
                objProcess.StartInfo.FileName = _sRuta
                objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal
                objProcess.StartInfo.Arguments = _sParámetros
                objProcess.Start()
                'Esperar hasta que el proceso termine. 
                If _bEsperar = True Then
                    objProcess.WaitForExit()
                End If

                'Liberación de recursos
                objProcess.Close()
                'Borrarmos el archivo si se ha indicado así.
                If _bBorrar Then
                    System.IO.File.Delete(_sRuta)
                End If
            Catch ex As Exception
                MsgBox("Error ejecutando " & _sRuta & " " & ex.Message, MsgBoxStyle.Critical, "EVolution Apps")
                _oLog.Log("Error: Ejecutando archivo " & _sRuta & ": " & ex.Message)
            End Try
        End Sub

        ''' <summary>
        ''' Borra el archivo o carpeta
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Borrar() As Boolean
            Dim _bResultado As Boolean = False

            Try
                If _Es_Archivo() Then
                    My.Computer.FileSystem.DeleteFile(_sRuta)
                    _bResultado = True
                End If
                If _Es_Directorio() Then
                    My.Computer.FileSystem.DeleteDirectory(Me.Ruta, FileIO.DeleteDirectoryOption.DeleteAllContents)
                    _bResultado = True
                End If
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Borrando archivo: " & ex.Message)
            End Try
            System.Threading.Thread.Sleep(10)
            System.Windows.Forms.Application.DoEvents()
            Return _bResultado
        End Function

        ''' <summary>
        ''' Lectura del archivo y lo almacena en contenido.
        ''' </summary>
        ''' <returns>verdadero o falso si la operación se ha realizado con éxito.</returns>
        ''' <remarks></remarks>
        Public Function Leer() As Boolean
            Dim _bResultado As Boolean = False

            Try
                _sContenido = My.Computer.FileSystem.ReadAllText(_sRuta)
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Leyendo archivo: " & ex.Message)
            End Try
            Return _bResultado
        End Function

        ''' <summary>
        ''' Graba el contenido en el archivo en modo sobreescritura.
        ''' </summary>
        ''' <returns>verdadero o falso si la operación se ha realizado con éxito.</returns>
        ''' <remarks></remarks>
        Public Function Grabar(Optional Windows As Boolean = False) As Boolean
            Dim _bResultado As Boolean = False

            Try
                If Windows Then
                    My.Computer.FileSystem.WriteAllText(_sRuta, _sContenido, False, System.Text.Encoding.ASCII)
                Else
                    My.Computer.FileSystem.WriteAllText(_sRuta, _sContenido, False, System.Text.Encoding.UTF8)
                End If
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Grabando archivo: " & ex.Message)
            End Try
            Return _bResultado
        End Function

        ''' <summary>
        ''' Copia un archivo.
        ''' </summary>
        ''' <param name="destino">ruta completa del archivo destino.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Copiar(ByVal destino As String) As Boolean
            Dim _bResultado As Boolean = False

            Try
                My.Computer.FileSystem.CopyFile(Me.Ruta, destino, True)
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Copiando archivo: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        ''' <summary>
        ''' Mueve el contenido
        ''' </summary>
        ''' <param name="Destino">Ruta del destinio.</param>
        ''' <returns>Devuelve verdadero si la operación ha sido realizado con éxito.</returns>
        ''' <remarks></remarks>
        Public Function Mover(ByVal Destino As String) As Boolean
            Dim _bResultado As Boolean = False

            Try
                My.Computer.FileSystem.MoveFile(_sRuta, Destino, True)
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Moviendo archivo: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        Public Function Mover(ByVal Origen As String, ByVal Destino As String) As Boolean
            Dim _bResultado As Boolean = False

            Try
                My.Computer.FileSystem.MoveFile(Origen, Destino, True)
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Moviendo archivo: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        ''' <summary>
        ''' Descomprime el archivo rar.
        ''' </summary>
        ''' <remarks></remarks>
        ''' <returns>Verdadero si la operación se ha realizado con éxito</returns>
        Public Function UnRAR(ByVal ruta_destino As String) As Boolean
            Dim objRegKey As RegistryKey
            Dim objArguments As String
            Dim _bResultado As Boolean = False

            objRegKey = Registry.ClassesRoot.OpenSubKey("WinRAR\Shell\Open\Command")
            'Windows 7 entrada de Registro para WinRAR Open Command
            Dim obj As Object = objRegKey.GetValue("")
            Dim objRarPath As String = obj.ToString()
            objRarPath = objRarPath.Substring(1, objRarPath.Length - 7)
            objRegKey.Close()
            ' Formato de la instrucción.
            ' " X G:\Downloads\samplefile.rar G:\Downloads\sampleextractfolder\"
            objArguments = " X " & " " & _sRuta & " " + " " + ruta_destino
            Dim objStartInfo As New ProcessStartInfo()
            'Establecer la propiedad de UseShellExecute StartInfo objeto a FALSO
            'Si no la podemos obtener el siguiente mensaje de error
            'El objeto del proceso debe tener la propiedad UseShellExecute establece en false para utilizar variables de entorno.

            Try
                objStartInfo.UseShellExecute = False
                objStartInfo.FileName = objRarPath
                objStartInfo.Arguments = objArguments
                objStartInfo.WindowStyle = ProcessWindowStyle.Hidden
                objStartInfo.WorkingDirectory = ruta_destino & "\"
                Dim objProcess As New Process()
                objProcess.StartInfo = objStartInfo
                objProcess.Start()

                _bResultado = True
            Catch ex As Exception
                _oLog.Log("Error: Descomprimiendo descomprimiendo rar: " & ex.Message)
            End Try
            Return _bResultado
        End Function

        ''' <summary>
        ''' Descomprime un archivo zip en la ruta especificada.
        ''' </summary>
        ''' <returns>Verdadero si la operación se ha realizado con éxito, falso se ha producido algún error.</returns>
        Public Function UnZIP(ruta_destino As String) As Boolean
            Dim _oZip As New ZipFile
            Dim _oAachivos As New ZipEntry
            Dim _bResultado As Boolean = False

            Try
                _oZip = ZipFile.Read(_sRuta)

                For Each _oArchivos In _oZip
                    _oArchivos.Extract(ruta_destino, ExtractExistingFileAction.OverwriteSilently)
                Next

                _bResultado = True
            Catch ex As Exception
                _oLog.Log("Error: Descomprimiendo zip: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        Public Function ComprimirCarpeta(ruta As String, Destino As String, Optional Password As String = "", Optional Cifrado As Boolean = False, Optional Comentario As String = "", Optional full As Boolean = False) As Boolean
            Dim _oArchivo As New cls_UAF
            Dim _oSistema As New cls_USF
            Dim _bResultado As Boolean = False
            Dim _oZip As New ZipFile
            Try
                _oZip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression
                If Cifrado Then _oZip.Encryption = EncryptionAlgorithm.WinZipAes256
                If Password <> "" Then _oZip.Password = Password
                If Comentario <> "" Then _oZip.Comment = ""
                If full Then
                    _oZip.AddDirectory(ruta)
                Else
                    For Each archivo As String In My.Computer.FileSystem.GetFiles(ruta, FileIO.SearchOption.SearchAllSubDirectories, "*.*")
                        If Not My.Computer.FileSystem.GetFileInfo(archivo).Extension = ".tmp" Then
                            _oZip.AddFile(archivo)
                        End If
                    Next
                End If

                _oArchivo.Ruta = Destino
                If _oArchivo.Existe Then _oArchivo.Borrar()
                _oZip.Save(Destino)

                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Comprimiendo carpeta: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        ''' <summary>
        ''' Crear una carpeta.
        ''' </summary>
        ''' <remarks>El nombre de la carpeta se fija en la propiedad ruta.</remarks>
        ''' <returns>Verdadero o false dependiendo del éxito de la operación.</returns>
        Public Function Crea_Directorio() As Boolean
            Dim _bResultado As Boolean = False

            Try
                My.Computer.FileSystem.CreateDirectory(Me.Ruta)
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Creando directorio: " & ex.Message)
            End Try
            Return _bResultado
        End Function
    End Class
End Namespace
