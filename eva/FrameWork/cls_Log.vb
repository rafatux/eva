﻿Imports eva
Imports System
Imports System.Management
Imports System.IO
Imports System.IO.File

Namespace FrameWork
    Public Class cls_Log
        Public Function Log(sCadena As String) As Boolean
            Dim filename As String = My.Application.Info.DirectoryPath & "\" & My.Application.Info.ProductName & ".log"
            Dim sw As StreamWriter = AppendText(filename)

            sw.WriteLine(Now() & ": " & sCadena)
            sw.Close()

            Return True
        End Function
    End Class
End Namespace
