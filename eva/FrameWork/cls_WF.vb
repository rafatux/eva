﻿Imports System.Net
Imports System.Web
Imports System.IO
Imports System.Text
Imports Newtonsoft.Json

Namespace FrameWork
    Public Class cls_WF
        Private _oLog As New eva.FrameWork.cls_Log
        ''' <summary>
        ''' Descarga un archivo usando protocolo http.
        ''' </summary>
        ''' <param name="url">Url del archivo a descargar.</param>
        ''' <param name="Ruta_archivo">Ruta y nombre del archivo donde ubicar la descarga.</param>
        Public Function Descarga_Archivos(ByVal url As String, ByVal Ruta_archivo As String, Optional Mostar_Aviso As Boolean = True) As Boolean
            Dim _oDescarga As New WebClient
            Dim _oArchivo As New eva.FrameWork.cls_UAF

            Dim _bResultado As Boolean = False

            _oArchivo.Ruta = Ruta_archivo
            If _oArchivo.Existe Then
                _oArchivo.Borrar()
            End If

            Try
                _oDescarga.DownloadFile(url, Ruta_archivo)
                _bResultado = True
            Catch ex As Exception
                _bResultado = False
                _oLog.Log("Error: Descargando archivo: " & ex.Message)
            End Try

            Return _bResultado
        End Function

        Public Function Subir_Archivo_PHP(url As String, nombre_variable As String, ruta_archivo As String) As String
            Dim _sResultado As String = ""

            Try
                Dim boundary As String = IO.Path.GetRandomFileName
                Dim header As New System.Text.StringBuilder()
                header.AppendLine("--" & boundary)
                header.Append("Content-Disposition: form-data; name=""" & nombre_variable & """;")
                header.AppendFormat("filename=""{0}""", IO.Path.GetFileName(ruta_archivo))
                header.AppendLine()
                header.AppendLine("Content-Type: application/octet-stream")
                header.AppendLine()

                Dim headerbytes() As Byte = System.Text.Encoding.UTF8.GetBytes(header.ToString)
                Dim endboundarybytes() As Byte = System.Text.Encoding.ASCII.GetBytes(vbNewLine & "--" & boundary & "--" & vbNewLine)

                Dim req As Net.HttpWebRequest = Net.HttpWebRequest.Create(url)
                req.ContentType = "multipart/form-data; boundary=" & boundary
                req.ContentLength = headerbytes.Length + New IO.FileInfo(ruta_archivo).Length + endboundarybytes.Length
                req.Method = "POST"

                Dim s As IO.Stream = req.GetRequestStream
                s.Write(headerbytes, 0, headerbytes.Length)
                Dim filebytes() As Byte = My.Computer.FileSystem.ReadAllBytes(ruta_archivo)
                s.Write(filebytes, 0, filebytes.Length)
                s.Write(endboundarybytes, 0, endboundarybytes.Length)
                s.Close()

                'Ejecutar petición y leer respuesta
                Dim Respuesta As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)
                Dim reader = New StreamReader(Respuesta.GetResponseStream())

                _sResultado = reader.ReadToEnd
            Catch ex As Exception
                _oLog.Log("Error: Subiendo archivo a través petición AJAX: " & ex.Message)
            End Try
            

            Return _sResultado
        End Function

        ''' <summary>
        ''' Devuelve el resultado de la petición de un archivo php con paso de parámetros usando POST.
        ''' </summary>
        ''' <param name="url">Url del script en PHP</param>
        ''' <param name="parámetros">Relación de parámetros usados por el script.</param>
        ''' <returns></returns>
        ''' <example>
        ''' <code>
        ''' 
        ''' </code>
        ''' </example>
        ''' <remarks>Los parámetros debe de tener el signo andpersand  delante del nombre</remarks>
        Public Function Parámetros_PHP(url As String, parámetros As String) As String
            Dim _sResultado As String = ""
            Dim postBuffer As Byte()

            ' Crear e inicializar la solicitud.
            Try
                Dim Solicitud As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
                Solicitud.UserAgent = "RafaTux"
                Solicitud.Method = "POST"

                'Escribe datos POST a la solicitud
                Solicitud.ContentType = "application/x-www-form-urlencoded; charset=UTF-8"

                'Se codifica a utf8 los parámetros
                postBuffer = System.Text.Encoding.UTF8.GetBytes(parámetros)
                Solicitud.ContentLength = postBuffer.Length
                Dim requestStream As Stream = Solicitud.GetRequestStream()
                requestStream.Write(postBuffer, 0, postBuffer.Length)
                requestStream.Close()

                'Ejecutar petición y leer respuesta
                Dim Respuesta As HttpWebResponse = DirectCast(Solicitud.GetResponse(), HttpWebResponse)
                Dim reader = New StreamReader(Respuesta.GetResponseStream())

                _sResultado = reader.ReadToEnd
            Catch ex As Exception
                _sResultado = "404"
                _oLog.Log("Error: Llamada petición AJAX: " & ex.Message)
            End Try

            postBuffer = Nothing

            Return _sResultado
        End Function

        ''' <summary>
        ''' Constructor de la clase.
        ''' </summary>
        ''' 
        Public Sub New()

        End Sub
    End Class

End Namespace
