﻿Option Explicit On
Option Strict On

Namespace FrameWork
    ''' <summary>
    ''' Gestión de SMS a través de la plataforma de ARSYS.
    ''' </summary>
    Public Class cls_SMS
        ''' <summary>
        ''' URL de formato de envio
        ''' </summary>
        Dim _sCadena_PHP_Envio As String = "https://sms.arsys.es/smsarsys/accion/enviarSms2.jsp?"
        Dim _sUsuario_emisor As String = ""
        Dim _sPassword_emisor As String = ""
        Dim _sNúmero_destino As String = ""
        Dim _sTexto As String = ""
        Dim _sID_Remitente As String = ""
        ''' <summary>
        ''' Almacena el resultado del último envio.
        ''' </summary>
        Dim _bResultado As Boolean = False
        Dim _sDescripción As String = ""
        ''' <summary>
        ''' Crédito de la cuenta
        ''' </summary>
        Dim _nCrédito As Integer = 0
        ''' <summary>
        ''' Identificativo de resultado.
        ''' </summary>
        Dim _idenvio As String = ""

        ''' <summary>
        ''' Identificador del usuario del servicio.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Usuario_Emisor As String
            Get
                Return _sUsuario_emisor
            End Get
            Set(ByVal value As String)
                _sUsuario_emisor = value
            End Set
        End Property

        ''' <summary>
        ''' Password del usuario del servicio.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Password_emisor As String
            Get
                Return _sPassword_emisor
            End Get
            Set(ByVal value As String)
                _sPassword_emisor = value
            End Set
        End Property

        ''' <summary>
        ''' Número de teléfono del destino.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Número_destino As String
            Get
                Return _sNúmero_destino
            End Get
            Set(ByVal value As String)
                _sNúmero_destino = value
            End Set
        End Property

        ''' <summary>
        ''' Mensaje a enviar.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Mensaje As String
            Get
                Return _sTexto
            End Get
            Set(ByVal value As String)
                _sTexto = value
            End Set
        End Property

        ''' <summary>
        ''' Texto que aparecerá como remitente.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Remitente As String
            Get
                Return _sID_Remitente
            End Get
            Set(ByVal value As String)
                _sID_Remitente = value
            End Set
        End Property

        ''' <summary>
        ''' Resultado de envío.
        ''' </summary>
        ''' <returns>Verdadero si ha sido enviado con éxito.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Resultado_envio As Boolean
            Get
                Return _bResultado
            End Get
        End Property

        ''' <summary>
        ''' Descripción del mensaje devuelto del servidor.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Descripción_envio As String
            Get
                Return _sDescripción
            End Get
        End Property

        ''' <summary>
        ''' Importe del crédido de la cuenta logeada.
        ''' </summary>
        ''' <value>Cantidad de mensajes que aún está en el crédito</value>
        Public ReadOnly Property Crédito_actual As Integer
            Get
                Return _nCrédito
            End Get
        End Property

        ''' <summary>
        ''' Identificador del sms enviado.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ID_envio As String
            Get
                Return _idenvio
            End Get
        End Property

        ''' <summary>
        ''' Envía el SMS
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Enviar()
            Dim _sCadena As String
            Dim _xml As New eva.AUBDF.cls_Registros_mdb
            Dim _oArchivo As New eva.FrameWork.cls_UAF

            _oArchivo.Ruta = "sms.xml"
            If _oArchivo.Existe Then
                _oArchivo.Borrar()
            End If
            _sCadena = _sCadena_PHP_Envio & "id=" & _sUsuario_emisor & "&psw=" & _sPassword_emisor & "&phoneNumber=" & _sNúmero_destino & "&textSms=" & _sTexto & "&remite=" & _sID_Remitente
            My.Computer.Network.DownloadFile(_sCadena, "sms.log")

            If _oArchivo.Existe Then
                _xml.BaseDeDatos = "sms.xml"
                _xml.ValorClave = "*"
                _xml.Tabla = "SendSMS"

                If _xml.Item("result").ToString = "OK" Then
                    _bResultado = True
                Else
                    _bResultado = False
                    MsgBox(_xml.Item("description").ToString, MsgBoxStyle.Information, "ERROR envio sms")
                End If
                _sDescripción = _xml.Item("description").ToString
                _nCrédito = CInt(_xml.Item("credit").ToString)
                _idenvio = _xml.Item("idenvio").ToString
            Else
                _bResultado = False
            End If
        End Sub

        ''' <summary>
        ''' Constructor de la clase
        ''' </summary>
        Public Sub New()

        End Sub

    End Class
End Namespace

