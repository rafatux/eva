﻿Option Explicit On
Option Strict On

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Text

Namespace FrameWork
    Public Class cls_XML
        Dim _dbTabla As DataSet
        Dim _sTabla As String
        Dim _nPuntero As Integer

        Public Function nodo_inicio(ByVal valor As String) As String
            Return "<" & valor & ">" & vbCrLf
        End Function

        Public Function nodo_fin(ByVal valor As String) As String
            Return "</" & valor & ">" & vbCrLf
        End Function

        Public Function tag(ByVal _tag As String, ByVal valor As Object, Optional ByVal longitud As Byte = 0, Optional ByVal fijo As Boolean = False) As String
            Dim _sResultado As String = ""

            Try
                If fijo Or (valor.ToString <> "" And Not IsDBNull(valor) And valor.ToString <> "0" And valor.ToString <> " " And valor.ToString <> "False" And valor.ToString <> "0.00" And valor.ToString <> "#0.00") Then
                    _sResultado = "<" & _tag & ">"


                    If valor.ToString <> "#0.00" Then
                        Select Case valor.GetType.Name
                            Case "Boolean"
                                If valor.ToString = "True" Then
                                    _sResultado = _sResultado & "1"
                                Else
                                    _sResultado = _sResultado & "0"
                                End If
                            Case "DateTime"
                                _sResultado = _sResultado & CDate(valor).Day & "/" & CDate(valor).Month & "/" & CDate(valor).Year
                            Case "String"
                                If longitud <> 0 Then
                                    _sResultado = _sResultado & Left(valor.ToString, longitud)
                                Else
                                    _sResultado = _sResultado & valor.ToString
                                End If
                            Case "Int32"
                                Select Case longitud
                                    Case 3
                                        _sResultado = _sResultado & "00" & valor.ToString
                                    Case 4
                                        _sResultado = _sResultado & "0" & valor.ToString
                                    Case Else
                                        _sResultado = _sResultado & valor.ToString
                                End Select
                            Case "Decimal"
                                Select Case longitud
                                    Case 3
                                        _sResultado = _sResultado & "00" & valor.ToString
                                    Case 4
                                        _sResultado = _sResultado & "0" & valor.ToString
                                    Case Else
                                        _sResultado = _sResultado & valor.ToString
                                End Select
                            Case "Integer"
                                Select Case longitud
                                    Case 3
                                        _sResultado = _sResultado & "00" & valor.ToString
                                    Case 4
                                        _sResultado = _sResultado & "0" & valor.ToString
                                    Case Else
                                        _sResultado = _sResultado & valor.ToString
                                End Select
                            Case Else
                                _sResultado = _sResultado & valor.ToString
                        End Select
                        _sResultado = _sResultado & "</" & _tag & ">" & vbCrLf
                    Else
                        _sResultado = _sResultado & "0</" & _tag & ">" & vbCrLf
                    End If

                End If
            Catch ex As Exception

            End Try

            Return _sResultado
        End Function

        Public Function conversión_boolean(ByVal valor As Boolean) As String
            Dim resultado As String = ""

            If valor Then
                resultado = "1"
            Else
                resultado = "0"
            End If

            Return resultado
        End Function

        Public Function Abrir_XML(Ruta_xml As String) As Boolean
            Dim _bResultado As Boolean = False
            Try
                _dbTabla = New Data.DataSet
                _dbTabla.ReadXml(Ruta_xml)
                _bResultado = True
            Catch ex As Exception

            End Try

            Return _bResultado
        End Function

        Public Property Puntero As Integer
            Get
                Return _nPuntero
            End Get
            Set(value As Integer)
                _nPuntero = value
            End Set
        End Property

        Public ReadOnly Property Población As Integer
            Get
                Return _dbTabla.Tables(_sTabla).Rows.Count
            End Get
        End Property

        Public Property Tabla As String
            Get
                Return _sTabla
            End Get
            Set(value As String)
                _sTabla = value
            End Set
        End Property

        Public Function Valor(Campo As String) As String
            Return _dbTabla.Tables(_sTabla).Rows(_nPuntero)(Campo).ToString
        End Function
    End Class
End Namespace