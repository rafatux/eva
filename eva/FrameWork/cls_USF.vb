﻿Option Explicit On
Option Strict On

Imports System
Imports System.Net
Imports System.Management
Imports System.Data
Imports System.IO
Imports System.Windows.Forms

Namespace FrameWork

    ''' <summary>
    ''' Gestiona los valores del sistema operativo.
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Class cls_USF

        ''' <summary>
        ''' Comprueba si el sistema es de 64 bits.
        ''' </summary>
        ''' <value>True si el sistema es de 32 bits.</value>
        Public ReadOnly Property es_64bits As Boolean
            Get
                Return System.Environment.Is64BitOperatingSystem
            End Get
        End Property

        ''' <summary>
        ''' Comprueba si el sistema es de 32 bits
        ''' </summary>
        ''' <value>true si el sistema es de 32 bits.</value>
        Public ReadOnly Property es_32bits As Boolean
            Get
                Return Not es_64bits
            End Get
        End Property

        Public ReadOnly Property ruta_carpeta_temporal As String
            Get
                Return System.Environment.GetEnvironmentVariable("TEMP")
            End Get
        End Property

        Public ReadOnly Property ruta_carpeta_windows As String
            Get
                Return System.Environment.GetEnvironmentVariable("WINDIR")
            End Get
        End Property

        Public ReadOnly Property Unidad_del_Sistema As String
            Get
                Return System.Environment.GetEnvironmentVariable("SYSTEMDRIVE")
            End Get
        End Property

        ''' <summary>
        ''' Ruta del archivo de programas de 16 bits en sistema 32 bits.
        ''' </summary>
        ''' <returns>Devuelve la ruta completa de la ruta de archivos de programas.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ruta_archivos_de_programas_x86 As String
            Get
                Return System.Environment.GetEnvironmentVariable("ProgramFiles(x86)").ToString
            End Get
        End Property

        ''' <summary>
        ''' Ruta de archivos de programas.
        ''' </summary>
        ''' <returns>Devuelve la ruta completa de la ruta de archivos de programas.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ruta_activos_de_programas As String
            Get
                Return System.Environment.GetEnvironmentVariable("ProgramFiles")
            End Get
        End Property

        Public ReadOnly Property ruta_actual As String
            Get
                Return System.Environment.CurrentDirectory.ToString
            End Get
        End Property

        ''' <summary>
        ''' Indica la primera ip del sistema.
        ''' </summary>
        ''' <value></value>
        ''' <returns>ip del sistema</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property mi_ip() As String
            Get
                Dim _host As String = System.Net.Dns.GetHostName
                Dim myIPs As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(_host)
                Dim _sResultado As String = myIPs.AddressList(0).ToString

                Return _sResultado
            End Get
        End Property

        ''' <summary>
        ''' Directorio de ips que está gestionado por el sistema.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Relación de ips en System.Net.IPHostEntry</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property mis_ips() As System.Net.IPHostEntry
            Get
                Dim _host As String = System.Net.Dns.GetHostName
                Dim myIPs As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(_host)

                Return myIPs
            End Get
        End Property

        ''' <summary>
        ''' Saber la versión del Sistema Operativo en la que se está ejecutando la aplicación.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property versión_windows() As String
            Get
                Return System.Environment.OSVersion.Version.ToString
            End Get
        End Property

        ''' <summary>
        ''' Devuelve la versión de services pack instalado.
        ''' </summary>
        ''' <returns>Devuelve la versión de services pack instalado.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property versión_services_pack() As String
            Get
                Return System.Environment.OSVersion.ServicePack.ToString
            End Get
        End Property

        ''' <summary>
        ''' Indica si el sistema es un windows XP
        ''' </summary>
        ''' <value>verdadero si se trata de un windows XP</value>
        Public ReadOnly Property es_windows_xp As Boolean
            Get
                Dim _bResultado As Boolean = False

                If Left(versión_windows, 3) = "5.1" Then
                    _bResultado = True
                End If

                Return _bResultado
            End Get
        End Property

        ''' <summary>
        ''' Indica que si el sistema es un windows 7
        ''' </summary>
        ''' <value>verdadero si se trata de un windows 7</value>
        Public ReadOnly Property es_windows_7 As Boolean
            Get
                Dim _bResultado As Boolean = False

                If Left(versión_windows, 3) = "6.1" Then
                    _bResultado = True
                End If

                Return _bResultado
            End Get
        End Property

        ''' <summary>
        ''' Cantidad de núcleos que posee el ordenador en la que se está ejecutando la aplicación.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property n_nucleos() As String
            Get
                Return System.Environment.ProcessorCount.ToString
            End Get
        End Property

        ''' <summary>
        ''' Nombre de la plataforma del ordenador que se está ejecutando la aplicación.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Plataforma() As String
            Get
                Return My.Computer.Info.OSPlatform.ToString
            End Get
        End Property

        ''' <summary>
        ''' Averigual el nombre del equipo.
        ''' </summary>
        ''' <value>Cadena con el nombre del equipo.</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Nombre_equipo() As String
            Get
                Return My.Computer.Name.ToString
            End Get
        End Property

        ''' <summary>
        ''' Devuelve el nombre del usuario activo.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Cadena con el nombre del usuario activo</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Nombre_Usuario As String
            Get
                Return System.Environment.UserName
            End Get
        End Property

        ''' <summary>
        ''' Saber el Id del microprocesador del ordenador que se está ejecutando la aplicación.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Procesador_ID() As String
            Get
                Dim strProcessorId As String = String.Empty
                Dim query As New SelectQuery("Win32_processor")
                Dim search As New ManagementObjectSearcher(query)
                Dim info As ManagementObject

                For Each info In search.Get()
                    strProcessorId = info("processorId").ToString()
                Next
                Return strProcessorId
            End Get
        End Property

        ''' <summary>
        ''' Cantidad de memoria física instalada en el ordenador en megas.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Memoria_física() As String
            Get
                Return (My.Computer.Info.TotalPhysicalMemory / (1024 * 1024)).ToString("##,#0")
            End Get
        End Property

        ''' <summary>
        ''' Cantidad de memoria disponible de la memoria instalada en el ordenador en megas.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Memoria_disponible() As String
            Get
                Return (My.Computer.Info.AvailablePhysicalMemory / (1024 * 1024)).ToString("##,#0")
            End Get
        End Property

        ''' <summary>
        ''' Dirección mac de la tarjeta de red por defecto
        ''' </summary>
        ''' <value>Dirección MAC de la tarjeta de red por defecto</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property MacAddress() As String
            Get
                Dim mc As ManagementClass = New ManagementClass("Win32_NetworkAdapterConfiguration")
                Dim moc As ManagementObjectCollection = mc.GetInstances()
                Dim MAddress As String = String.Empty
                For Each mo As ManagementObject In moc

                    If (MAddress.Equals(String.Empty)) Then
                        If CBool(mo("IPEnabled")) Then MacAddress = mo("MacAddress").ToString()

                        mo.Dispose()
                    End If
                    MacAddress = MAddress.Replace(":", String.Empty)
                Next
                Return MAddress
            End Get
        End Property

        ''' <summary>
        ''' Número de serie del disco duro principal.
        ''' </summary>
        ''' <param name="strDriveLetter"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Disco_duro_serial(Optional ByVal strDriveLetter As String = "C") As String
            Dim disk As ManagementObject = New ManagementObject(String.Format("win32_logicaldisk.deviceid=""{0}:""", strDriveLetter))
            disk.Get()

            Return disk("VolumeSerialNumber").ToString()
        End Function

        ''' <summary>
        ''' Identificativo de la placa base instalada.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Placa_base_ID() As String
            Get
                Dim strMotherBoardID As String = String.Empty
                Dim query As New SelectQuery("Win32_BaseBoard")
                Dim search As New ManagementObjectSearcher(query)
                Dim info As ManagementObject
                For Each info In search.Get()
                    strMotherBoardID = info("SerialNumber").ToString()
                Next
                Return strMotherBoardID
            End Get
        End Property

        ''' <summary>
        ''' Mata todos los procesos que se están ejecutando en memoria con el nombre indicado.
        ''' </summary>
        ''' <param name="proceso">Nombre del proceso a eliminar de la memoria del sistema.</param>
        Public Sub mata_proceso(ByVal proceso As String)

            Dim myProcesses() As Process
            Dim myProcess As Process

            myProcesses = System.Diagnostics.Process.GetProcessesByName(proceso)
            ' En caso de que existiera mas de un proceso con el mismo nombre, ej:
            ' "Notepad" ha sido abierto mas de una vez
            Try
                For Each myProcess In myProcesses
                    myProcess.CloseMainWindow()
                    myProcess.Kill()
                    myProcess.WaitForExit()
                Next

            Catch ex As Exception

            End Try
        End Sub

        Public Sub pausa(ByVal milisegundos As Integer)
            System.Threading.Thread.Sleep(milisegundos)
        End Sub

        Public Function Portapapeles_contiene_texto() As Boolean
            Return Clipboard.ContainsText
        End Function

        Public Function Portapapeles_pegar_texto() As String
            Return Clipboard.GetDataObject.GetData("Text").ToString
        End Function

        Public Sub Crear_Variable_Entorno_Por_Usuario(variable As String, valor As String)
            Environment.SetEnvironmentVariable(variable, valor, EnvironmentVariableTarget.User)
        End Sub

        Public Sub Crear_Variable_Entorno_Por_Máquina(variable As String, valor As String)
            Environment.SetEnvironmentVariable(variable, valor, EnvironmentVariableTarget.Machine)
        End Sub

        Public Sub Crear_Variable_Entorno_Por_Proceso(variable As String, valor As String)
            Environment.SetEnvironmentVariable(variable, valor, EnvironmentVariableTarget.Process)
        End Sub

        Public Function Leer_Variable_Entorno(variable As String) As String
            Return Environment.GetEnvironmentVariable(variable)
        End Function

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace