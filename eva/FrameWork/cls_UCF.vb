﻿Option Explicit On
Option Strict On

Imports System.Text.RegularExpressions


Namespace FrameWork
    ''' <summary>
    ''' Utilidades para el tratamiento de cadenas.
    ''' </summary>
    Public Class cls_UCF

        ''' <summary>
        ''' Creación de una cadena de textos con tanto espacios deseados.
        ''' </summary>
        ''' <param name="nCantidad">Número de espacios en blanco.</param>
        ''' <returns>cadena con estacios en blanco.</returns>
        Public Function CrearEspacios(ByVal nCantidad As Integer) As String
            Dim _sResultado As String = ""
            Dim _nContador As Integer = 0

            While _nContador < nCantidad
                _sResultado = _sResultado & " "
                _nContador = _nContador + 1
            End While

            Return _sResultado
        End Function

        ''' <summary>
        ''' Añade ceros a la cadena introducida hasta la longitud deseada
        ''' </summary>
        ''' <remarks>Si la cadena introducida como parámetro es inferior en longitud que la expresada como parámetro, se devuelve la cadena de entrada.</remarks>
        ''' <param name="cadena">Cadena a completar con ceros.</param>
        ''' <param name="longitud">Longitud máxima que tendrá la cadena resultante.</param>
        ''' <returns>cadena con los ceros añadidos.</returns>
        Public Function CrearCerosIzq(ByVal cadena As String, ByVal longitud As Integer) As String
            Dim _sResultado As String = ""
            Dim _nContador As Integer = 0

            While _nContador < longitud - Len(cadena)
                _sResultado = _sResultado & "0"
                _nContador = _nContador + 1
            End While

            Return _sResultado & cadena
        End Function

        ''' <summary>
        ''' Añade espacios a la cadena introducida hasta la longitud deseada
        ''' </summary>
        ''' <remarks>Si la cadena introducida como parámetro es inferior en longitud que la expresada como parámetro, se devuelve la cadena de entrada.</remarks>
        ''' <param name="cadena">Cadena a completar con ceros.</param>
        ''' <param name="longitud">Longitud máxima que tendrá la cadena resultante.</param>
        ''' <returns>cadena con los ceros añadidos.</returns>
        Public Function CrearEspaciosIzq(ByVal cadena As String, ByVal longitud As Integer) As String
            Dim _sResultado As String = ""
            Dim _nContador As Integer = 0

            While _nContador < longitud - Len(cadena)
                _sResultado = _sResultado & " "
                _nContador = _nContador + 1
            End While

            Return _sResultado & cadena
        End Function

        Public Function FormatoMoneda(ByVal cantidad As String) As String
            Dim _sResultado As String = ""

            If IsNumeric(cantidad) Then
                _sResultado = Format(CSng(cantidad), "#0.00").Replace(",", ".")
            End If

            Return _sResultado
        End Function

        Public Function FormatoDecimal(ByVal cantidad As String) As String
            Dim _sResultado As String = ""

            If IsNumeric(cantidad) Then
                _sResultado = Format(CSng(cantidad), "#0.00").Replace(",", ".")
            End If

            Return _sResultado
        End Function

        ''' <summary>
        ''' Devuelve la fecha enviada como parámetro en formato yyyymmdd
        ''' </summary>
        ''' <param name="fecha">Fecha en formato normal</param>
        ''' <returns>Cadena con ela fecha formateada.</returns>
        Public Function conversión_fecha_yyyymmdd(ByVal fecha As String) As String
            Dim _sResultado As String = ""
            Dim _sDía As String = ""
            Dim _sMes As String = ""
            Dim _sAnno As String = ""

            _sAnno = CrearCerosIzq(CDate(fecha).Year.ToString, 4)
            _sMes = CrearCerosIzq(CDate(fecha).Month.ToString, 2)
            _sDía = CrearCerosIzq(CDate(fecha).Day.ToString, 2)

            Return _sAnno & "-" & _sMes & "-" & _sDía
        End Function

        ''' <summary>
        ''' Devuelve la fecha enviada como parámetro en formato yyyymmdd
        ''' </summary>
        ''' <param name="fecha">Fecha en formato normal</param>
        ''' <returns>Cadena con ela fecha formateada.</returns>
        Public Function conversión_fecha_yyyymmdd(ByVal fecha As Date) As String
            Dim _sResultado As String = ""
            Dim _sDía As String = ""
            Dim _sMes As String = ""
            Dim _sAnno As String = ""

            _sAnno = CrearCerosIzq(fecha.Year.ToString, 4)
            _sMes = CrearCerosIzq(fecha.Month.ToString, 2)
            _sDía = CrearCerosIzq(fecha.Day.ToString, 2)

            Return _sAnno & "-" & _sMes & "-" & _sDía
        End Function

        Public Function conversión_fecha_cadenaSQL(ByVal fecha As Date) As String
            Dim _sResultado As String = ""
            Dim _sDía As String = ""
            Dim _sMes As String = ""
            Dim _sAnno As String = ""

            _sAnno = fecha.Year.ToString
            _sMes = fecha.Month.ToString
            _sDía = fecha.Day.ToString

            Return _sDía & "/" & _sMes & "/" & _sAnno
        End Function

        Public Function ES_Trimestre_Actual() As Byte
            Dim _nResultado As Byte = 0
            Dim _oFecha As Date = Now
            If _oFecha.Month > 1 And _oFecha.Month < 4 Then
                _nResultado = 1
            End If

            If _oFecha.Month > 4 And _oFecha.Month < 7 Then
                _nResultado = 2
            End If

            If _oFecha.Month > 7 And _oFecha.Month < 10 Then
                _nResultado = 3
            End If

            If _oFecha.Month > 10 Then
                _nResultado = 4
            End If
            Return _nResultado
        End Function

        Public Function Fecha_Inicio_Trimestre(trimestre As Byte) As Date
            Dim _dResultado As Date = Now

            Select Case trimestre
                Case 1
                    _dResultado = CDate("01/01/" & Now.Year)
                Case 2
                    _dResultado = CDate("01/04/" & Now.Year)
                Case 3
                    _dResultado = CDate("01/07/" & Now.Year)
                Case 4
                    _dResultado = CDate("01/10/" & Now.Year)
            End Select

            Return _dResultado
        End Function

        Public Function Fecha_Final_Trimetres(trimestre As Byte) As Date
            Dim _dResultado As Date = Now

            Select Case trimestre
                Case 1
                    _dResultado = CDate("31/03/" & Now.Year)
                Case 2
                    _dResultado = CDate("30/06/" & Now.Year)
                Case 3
                    _dResultado = CDate("30/09/" & Now.Year)
                Case 4
                    _dResultado = CDate("31/12/" & Now.Year)
            End Select

            Return _dResultado
        End Function

        Public Function str2sng(valor As String) As Single
            Dim _nResultado As Single

            Try
                _nResultado = CSng(valor.Replace(".", ","))
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function str2uint(valor As String) As UInteger
            Dim _nResultado As UInteger

            Try
                _nResultado = CUInt(valor)
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function str2int(valor As String) As Integer
            Dim _nResultado As Integer

            Try
                _nResultado = CInt(valor)
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function Es_Decimal(valor As String) As Boolean
            Dim _bResultado As Boolean = True
            Dim _nTemp As Single = 0

            Try
                _nTemp = CSng(valor)
            Catch ex As Exception
                _bResultado = False
            End Try

            Return _bResultado
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="valor"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Es_Entero(valor As String) As Boolean
            Dim _bResultado As Boolean = True
            Dim _nTemp As Integer = 0

            Try
                If InStr(valor, ",") <> 0 Or InStr(valor, ".") <> 0 Then
                    _bResultado = False
                End If
                _nTemp = CInt(valor)
            Catch ex As Exception
                _bResultado = False
            End Try

            Return _bResultado
        End Function

        Public Function Es_Email(valor As String) As Boolean
            Dim _bResultado As Boolean = False
            Dim pattern As String = "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"

            If valor = "" Then
                _bResultado = True
            Else
                If Regex.IsMatch(valor, pattern) Then
                    _bResultado = True
                Else
                    _bResultado = False
                End If
            End If

            Return _bResultado
        End Function

        Public Function toSng(valor As String) As Single
            Dim _nResultado As Single

            Try
                _nResultado = CSng(valor.Replace(".", ","))
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function NoNuloSng(valor As Object) As Single
            Dim _nResultado As Single

            Try
                _nResultado = CSng(valor.ToString.Replace(".", ","))
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function NoNuloUInt(valor As Object) As UInteger
            Dim _nResultado As UInteger

            Try
                _nResultado = CUInt(valor)
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function NoNuloInt(valor As Object) As Integer
            Dim _nResultado As Integer

            Try
                _nResultado = CInt(valor)
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function NoNuloByte(valor As Object) As Byte
            Dim _nResultado As Byte

            Try
                _nResultado = CByte(valor)
            Catch ex As Exception
                _nResultado = 0
            End Try

            Return _nResultado
        End Function

        Public Function NoNuloStr(valor As Object) As String
            Dim _sResultado As String

            If Not IsDBNull(valor) Then
                Try
                    _sResultado = valor.ToString
                Catch ex As Exception
                    _sResultado = ""
                End Try

            Else
                _sResultado = ""
            End If

            Return _sResultado
        End Function

        Public Function NoNuloBol(valor As Object) As Boolean
            Dim _bResultado As Boolean

            If Not IsDBNull(valor) Then
                _bResultado = CBool(valor)
            Else
                _bResultado = False
            End If

            Return _bResultado
        End Function

        Public Function NoNuloDat(valor As Object) As Date
            Dim _dResultado As Date

            If Not IsDBNull(valor) Then
                _dResultado = CDate(valor)
            Else
                _dResultado = Now
            End If

            Return _dResultado
        End Function

        Public Function SQL_Número(valor As Object) As String
            Return valor.ToString.Replace(",", ".")
        End Function

        Public Function SQL_Cadena(valor As Object) As String
            Return "'" & valor.ToString & "'"
        End Function

        Public Function SQL_Booleano(valor As Object) As String
            Dim _bResultado As String = "0"

            If CBool(valor) Then _bResultado = "1"

            Return _bResultado
        End Function

        Public Function SQL_Fecha(valor As Object) As String
            Return "'" & conversión_fecha_yyyymmdd(CDate(valor)) & "'"
        End Function

    End Class
End Namespace